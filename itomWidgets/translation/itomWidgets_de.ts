<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DoubleRangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MenuComboBox</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>MenuComboBoxPrivate</name>
    <message>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>MotorAxisController</name>
    <message>
        <source>MotorAxisController</source>
        <translation></translation>
    </message>
    <message>
        <source>Absolute movement</source>
        <translation>Absolute Fahrt</translation>
    </message>
    <message>
        <source>Relative movement</source>
        <translation>Relative Fahrt</translation>
    </message>
    <message>
        <source>All movements</source>
        <translation>Absolut und Relativ</translation>
    </message>
    <message>
        <source>Only monitor</source>
        <translation>Nur überwachen</translation>
    </message>
    <message>
        <source>Start all axes</source>
        <translation>Alle Achsen starten</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <source>Current Pos.</source>
        <translation>Aktuelle Pos.</translation>
    </message>
    <message>
        <source>Target Pos.</source>
        <translation>Zielposition</translation>
    </message>
    <message>
        <source>Abs.</source>
        <translation></translation>
    </message>
    <message>
        <source>Step Size</source>
        <translation>Schrittweite</translation>
    </message>
    <message>
        <source>Rel.</source>
        <translation></translation>
    </message>
    <message>
        <source>up</source>
        <translation type="unfinished">Auf</translation>
    </message>
    <message>
        <source>down</source>
        <translation type="unfinished">Ab</translation>
    </message>
    <message>
        <source>go</source>
        <translation type="unfinished">Start</translation>
    </message>
    <message>
        <source>slot &apos;requestStatusAndPosition&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;requestStatusAndPosition&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Der Pointer des Plugins ist ungültig.</translation>
    </message>
    <message>
        <source>slot &apos;setPosAbs&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setPosAbs&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>slot &apos;%s&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;%s&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung. Die Plugin-Instanz antwortet nicht.</translation>
    </message>
    <message>
        <source>Error while calling &apos;%1&apos;</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Warning while calling &apos;%1&apos;</source>
        <translation type="unfinished">Warnung beim Aufruf von &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation type="unfinished">Einheit</translation>
    </message>
    <message>
        <source>Decimals</source>
        <translation type="unfinished">Dezimal</translation>
    </message>
    <message>
        <source>axisIndex is out of bounds.</source>
        <translation type="unfinished">Der Achsenindex liegt außerhalb des Gültigkeitsbereichs.</translation>
    </message>
    <message>
        <source>refresh</source>
        <translation type="unfinished">Aktualisieren</translation>
    </message>
    <message>
        <source>Actuator not available</source>
        <translation type="unfinished">Der Motor ist nicht verfügbar</translation>
    </message>
    <message>
        <source>interrupt movement</source>
        <translation type="unfinished">Fahrt unterbrochen</translation>
    </message>
    <message>
        <source>start movement</source>
        <translation type="unfinished">Fahrt starten</translation>
    </message>
</context>
<context>
    <name>ParamEditorWidget</name>
    <message>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; existiert nicht.</translation>
    </message>
    <message>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Der Pointer des Plugins ist ungültig.</translation>
    </message>
    <message>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Setzen des Parameters</translation>
    </message>
    <message>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Beim Setzen des Parameters ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; existiert nicht.</translation>
    </message>
    <message>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung. Die Plugin-Instanz antwortet nicht.</translation>
    </message>
    <message>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <source>Warning while execution</source>
        <translation type="unfinished">Bei der Ausführung ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">Allgemein</translation>
    </message>
</context>
<context>
    <name>PathLineEdit</name>
    <message>
        <source>Select a file to save </source>
        <translation>Dateiname angeben</translation>
    </message>
    <message>
        <source>Open a file</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <source>Select a directory...</source>
        <translation>Ein Verzeichnis auswählen...</translation>
    </message>
    <message>
        <source>Open a dialog</source>
        <translation type="unfinished">Dialog öffnen</translation>
    </message>
</context>
<context>
    <name>PathLineEditPrivate</name>
    <message>
        <source>Open a dialog</source>
        <translation type="vanished">Dialog öffnen</translation>
    </message>
</context>
<context>
    <name>PlotInfoMarker</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Marker %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlotInfoPicker</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>PlotInfoShapes</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation type="unfinished">Punkt &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <source>Line %1</source>
        <translation type="unfinished">Linie %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <source>End</source>
        <translation type="unfinished">Ende</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="unfinished">Länge</translation>
    </message>
    <message>
        <source>Circle %1</source>
        <translation type="unfinished">Kreis %1</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="unfinished">Zentrum</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation></translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation></translation>
    </message>
    <message>
        <source>Ellipse %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Radius 1</source>
        <translation></translation>
    </message>
    <message>
        <source>Radius 2</source>
        <translation></translation>
    </message>
    <message>
        <source>Side Length</source>
        <translation type="unfinished">Seitenlänge</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished">Breite</translation>
    </message>
    <message>
        <source>Height</source>
        <translation type="unfinished">Höhe</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation type="unfinished">Knoten</translation>
    </message>
</context>
<context>
    <name>PythonLogWidget</name>
    <message>
        <source>This widget automatically display the selected type of python output or error messages.</source>
        <translation type="unfinished">Dieses Widget zeigt automatisch die optional auswählbaren Arten von Python-Nachrichten (Ausgaben und/oder Fehlermeldungen) an.</translation>
    </message>
    <message>
        <source>Errors are displayed in red.</source>
        <translation type="unfinished">Fehler werden in rot dargestellt.</translation>
    </message>
    <message>
        <source>itom plot api not available</source>
        <translation type="unfinished">Die itom-Plot-API ist nicht verfügbar</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished">Bereinigen</translation>
    </message>
    <message>
        <source>Auto Scroll</source>
        <translation type="unfinished">Auto-Scroll</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Basic</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchBoxPrivate</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>SliderWidget</name>
    <message>
        <source>SliderWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>brushCreatorDialog</name>
    <message>
        <source>Brush Style</source>
        <translation type="unfinished">Pinselstil</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="unfinished">Farbe</translation>
    </message>
</context>
<context>
    <name>penCreatorDialog</name>
    <message>
        <source>Pen Style</source>
        <translation type="unfinished">Stiftstil</translation>
    </message>
    <message>
        <source>Join Style</source>
        <translation type="unfinished">Join-Stil</translation>
    </message>
    <message>
        <source>Cap Style</source>
        <translation type="unfinished">Cap-Stil</translation>
    </message>
    <message>
        <source>Line Width</source>
        <translation type="unfinished">Linientiefe</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="unfinished">Farbe</translation>
    </message>
    <message>
        <source>Line Style</source>
        <translation type="unfinished">Linienstil</translation>
    </message>
</context>
</TS>
