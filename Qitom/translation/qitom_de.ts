<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogAboutQItom</name>
    <message>
        <source>itom logo</source>
        <translation type="vanished">itom Logo</translation>
    </message>
    <message>
        <location filename="../ui/dialogAbout.ui" line="+44"/>
        <location line="+252"/>
        <source>ito logo</source>
        <translation>ITO Logo</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:6.6pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; color:#000000;&quot;&gt;Institut fuer Technische Optik (&lt;/span&gt;&lt;a href=&quot;http://www.ito.uni-stuttgart.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#000000;&quot;&gt;ITO&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt; color:#000000;&quot;&gt;) Universitaet Stuttgart Prof. Dr. Wolfgang Osten Pfaffenwaldring 9, 70569 Stuttgart, Germany&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Copy all version numbers to clip board</source>
        <translation>Kopiert alle Versionsnummern in die Zwischenablage</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Versions</source>
        <translation>Versionen kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location line="-419"/>
        <source>About itom</source>
        <translation>Über itom</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Basic information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+62"/>
        <location line="+78"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:6.6pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-122"/>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>The following license text corresponds to the file COPYING.txt, which is provided with this installation of itom. </source>
        <translation>Die folgende Lizenz entspricht der Datei COPYING.txt, die mit dieser Installation von itom mitgeliefert wurde.</translation>
    </message>
</context>
<context>
    <name>DialogCloseItom</name>
    <message>
        <location filename="../ui/dialogCloseItom.ui" line="+20"/>
        <source>Close itom</source>
        <translation>itom schließen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>itom cannot be closed since Python is still running.</source>
        <translation>itom kann nicht geschlossen werden während Python noch ausgeführt wird.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Do you want to interrupt Python to close itom?</source>
        <translation>Soll Python abgebrochen werden um itom zu schließen?</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Interrupt Python</source>
        <translation>Python abbrechen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%v s</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogEditBreakpoint</name>
    <message>
        <location filename="../ui/dialogEditBreakpoint.ui" line="+17"/>
        <source>Edit Breakpoint</source>
        <translation>Haltepunkt editieren</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Line-Number:</source>
        <translation>Zeilennummer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Condition:</source>
        <translation>&amp;Bedingungen:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&amp;Ignore Count:</source>
        <translation>&amp;Ignorierungen:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>temporary &amp;breakpoint</source>
        <translation>&amp;Temporärer Haltepunkt</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;enabled</source>
        <translation>&amp;Aktivierung</translation>
    </message>
</context>
<context>
    <name>DialogGoto</name>
    <message>
        <location filename="../ui/dialogGoto.ui" line="+14"/>
        <source>Go To</source>
        <translation>Gehe zu</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>line</source>
        <translation>Zeile</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>character</source>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Line number (0 - 0, current: 0)</source>
        <translation>Zeilennummer (0 - 0, aktuell: 0)</translation>
    </message>
</context>
<context>
    <name>DialogIconBrowser</name>
    <message>
        <location filename="../ui/dialogIconBrowser.ui" line="+14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Insert and Close</source>
        <translation>Einfügen und Schließen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Copy to Clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogLoadedPlugins</name>
    <message>
        <location filename="../ui/dialogLoadedPlugins.ui" line="+14"/>
        <source>Load Status of Plugins</source>
        <translation>Ladestatus der Plugins</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Errors</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Warnings</source>
        <translation>Warnungen</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Messages</source>
        <translation>Meldungen</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Plugin name</source>
        <translation>Plugin-Name</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Ignored</source>
        <translation>Ignoriert</translation>
    </message>
    <message>
        <location line="-232"/>
        <source>Load status of detected plugin files</source>
        <translation>Ladestatus der gefundenen Plugin-Dateien</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only display compatible plugins (debug / release)</source>
        <translation>Nur kompatible Plugins anzeigen (debug/release)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Collapse all</source>
        <translation>Alles reduzieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Expand all</source>
        <translation>Alles erweitern</translation>
    </message>
</context>
<context>
    <name>DialogNewPluginInstance</name>
    <message>
        <location filename="../ui/dialogNewPluginInstance.ui" line="+14"/>
        <source>New Plugin Instance</source>
        <translation>Neue Plugin-Instanz</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Plugin Information</source>
        <translation>Plugin-Informationen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[type]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Access instance with python</source>
        <translation>Zugriff auf Instanz mt Python</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Variable name in global workspace</source>
        <translation>Variablenname im globalen Workspace</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable name:</source>
        <translation>Variablenname:</translation>
    </message>
</context>
<context>
    <name>DialogOpenFileWithFilter</name>
    <message>
        <location filename="../ui/dialogOpenFileWithFilter.ui" line="+14"/>
        <source>File Import Assistant</source>
        <translation>Assistent für den Import von Dateien</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[filter]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&lt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>file is being loaded...</source>
        <translation>Datei wird geladen...</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save in global workspace</source>
        <translation>Als globale Variable speichern</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Variable name in global workspace</source>
        <translation>Variablenname im globalen Workspace</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Variable name:</source>
        <translation>Variablenname:</translation>
    </message>
</context>
<context>
    <name>DialogOpenNewGui</name>
    <message>
        <location filename="../ui/dialogOpenNewGui.ui" line="+14"/>
        <source>Open plugin widget</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Widget Information</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>[name]</source>
        <translation>[Name]</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
</context>
<context>
    <name>DialogPipManager</name>
    <message>
        <location filename="../ui/dialogPipManager.ui" line="+14"/>
        <source>Python Package Manager</source>
        <translation>Python Paketmanager</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Installed Packages</source>
        <translation>Installierte Pakete</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Install...</source>
        <translation>Installieren...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update...</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Uninstall</source>
        <translation>Deinstallieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sudo Uninstall</source>
        <translation>Als &apos;Sudo&apos; deinstallieren</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Auf Updates prüfen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pip Settings</source>
        <translation>Pip-Einstellungen</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No connection to pip available</source>
        <translation>Keine Verbindung zu Pip verfügbar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy in the form [user:passwd@]proxy.server:port</source>
        <translation>Proxy-Angaben in der Form: [Benutzer:Passwort@]Proxy.Server:Port</translation>
    </message>
    <message>
        <source>[user:passwd]@proxy.server:port</source>
        <translation type="vanished">[Benutzer:Passwort@]Proxy.Server:Port</translation>
    </message>
    <message>
        <location line="-112"/>
        <source>Check For Updates</source>
        <translation>Nach Updates suchen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Verify installed packages have compatible dependencies.</source>
        <translation type="unfinished">Kompatibilitätsabhängigkeiten installierter Pakete prüfen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify Installed Packages</source>
        <translation type="unfinished">Installierte Pakete prüfen</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:6.6pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+65"/>
        <source>[user:passwd@]proxy.server:port</source>
        <translation>[Benutzer:Passwort@]Proxy.Server:Port</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Timeout:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+33"/>
        <source>socket timeout (default: 15 s)</source>
        <translation>Wartezeit beim Verbindungsaufbau (Standard: 15 s)</translation>
    </message>
    <message>
        <location line="-27"/>
        <source> s</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Run pip in an isolated mode, ignoring environment variables and user configuration.</source>
        <translation>Pip im Isulationsmodus starten (Umgebungsvariabeln und Benutzerkonfiguration werden ignoriert).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run pip in an isolated mode</source>
        <translation>Pip im Isulationsmodus starten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Retries:</source>
        <translation>Wiederholungen:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Trusted Hosts:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Semicolon-separated list of hosts. These hosts are trusted (avoiding SSL security issues).</source>
        <translation type="unfinished">Host-Liste mit Semikolon getrennt. Diese Hosts sind  &quot;trusted&quot; (SSL ist zu vermeiden).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>pypi.python.org; files.pythonhosted.org; pypi.org</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Hint: From some networks, you also have to properly set the environment variables http_proxy and https_proxy!</source>
        <translation type="unfinished">Hinweis: Bei einigen Netzwerken müssen auch die Umgebungsvariablen http_proxy und https_proxy gesetzt werden!</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Start itom</source>
        <translation>itom starten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogPipManagerInstall</name>
    <message>
        <location filename="../ui/dialogPipManagerInstall.ui" line="+14"/>
        <source>Install Package</source>
        <translation>Paket installieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Install</source>
        <translation>Installation</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Install from Wheel archive (whl)</source>
        <translation>Von Wheel-Archiv (whl) installieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Install from tar.gz or zip archive</source>
        <translation>Von tar.gz oder zip-Archiv installieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search Python package index for package name, download and install it</source>
        <translation>Python-Paket manuell herunterladen und installieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Package file or name:</source>
        <translation>Paketdatei oder Paketname:</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+60"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Options (Basic pip options are also considered)</source>
        <translation>Optionen (Enthält auch Basisoptionen für Pip)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Upgrade existing package if newer version is available</source>
        <translation>Aktualisieren wenn eine neuere Version verfügbar ist</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Install dependencies if required (if upgrade flag is set, dependencies will be updated, too)</source>
        <translation>Installation von abhängigen Modulen wenn nötig (bei Upgrades werden Abhängigkeiten auch aktualisiert)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>If checked, indicate an URL or a local path that is searched for the indicated package</source>
        <translation>Eine URL oder ein lokales Verzeichnis, in dem nach dem Paket gesucht wird</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find links:</source>
        <translation>Suchpfad:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Ignore Python package index (pypi.python.org/simple). Only look at find-links URLs.</source>
        <translation>Online Python-Pakete ignorieren (pypi.python.org/simple). Es wird nur der Suchpfad berücksichtigt.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Run install with sudo rights (linux only)</source>
        <translation>Installation mit &apos;sudo&apos;-Berechtigungen ausführen (nur Linux)</translation>
    </message>
</context>
<context>
    <name>DialogPluginPicker</name>
    <message>
        <location filename="../ui/dialogPluginPicker.ui" line="+14"/>
        <source>Plugin Picker</source>
        <translation>Plugin laden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Choose plugin instance:</source>
        <translation>Plugin-Instanz auswählen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>show plugins without active instances</source>
        <translation>Plugins ohne aktive Instanzen anzeigen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>create new instance</source>
        <translation>Neue Instanz erstellen</translation>
    </message>
</context>
<context>
    <name>DialogReloadModule</name>
    <message>
        <location filename="../ui/dialogReloadModule.ui" line="+14"/>
        <source>Python Modules</source>
        <translation>Python-Module</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>show build-in modules</source>
        <translation>BuildIn-Module anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show modules lying in python-folder(s)</source>
        <translation>Module im Python-Ordner anzeigen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Module Name:</source>
        <translation>Modul-Name:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;click on item to see information&gt;</source>
        <translation>&lt;für Informationen oben einen Eintrag auswählen&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Path:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reload Modules</source>
        <translation>Modul neu laden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogReplace</name>
    <message>
        <location filename="../ui/dialogReplace.ui" line="+32"/>
        <source>Find and Replace</source>
        <translation>Suchen und Ersetzen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Find what</source>
        <translation>Suchen nach</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Replace with</source>
        <translation>Ersetzen durch</translation>
    </message>
    <message>
        <source>Find in</source>
        <translation type="vanished">Suchen in</translation>
    </message>
    <message>
        <source>Current Document</source>
        <translation type="vanished">Aktuelles Dokument</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="vanished">Markierung</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Find Next</source>
        <translation>Weitersuchen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Replace</source>
        <translation>Ersetzen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Replace All</source>
        <translation>Alle ersetzen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In Selection</source>
        <translation>In Markierung</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Expand</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Match whole word only</source>
        <translation>Nur ganzes Wort suchen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Match case</source>
        <translation>Groß-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Wrap around</source>
        <translation>Vom Ende zum Anfang</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Regular expression</source>
        <translation>Reguläre Ausdrücke</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Direction</source>
        <translation>Suchrichtung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Up</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Down</source>
        <translation>Abwärts</translation>
    </message>
</context>
<context>
    <name>DialogSaveFileWithFilter</name>
    <message>
        <location filename="../ui/dialogSaveFileWithFilter.ui" line="+14"/>
        <source>File Export Assistant</source>
        <translation>Assistent für den Export von Dateien</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[filter]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
</context>
<context>
    <name>DialogSelectUser</name>
    <message>
        <location filename="../ui/dialogSelectUser.ui" line="+67"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Run itom as...</source>
        <translation>Unter folgendem Benutzer starten...</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Permission</source>
        <translation>Berechtigungen</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Ini File</source>
        <translation>Ini-Datei</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Select user</source>
        <translation>Benutzer auswählen</translation>
    </message>
</context>
<context>
    <name>DialogSnapshot</name>
    <message>
        <location filename="../ui/dialogSnapshot.ui" line="+14"/>
        <source>Snapshot Tool</source>
        <translation>Aufnahmedialog</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Live Image</source>
        <translation>Live-Bild</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save Data</source>
        <translation>Speicheroptionen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Save Image After Snap</source>
        <translation>Bild nach Aufnahme speichern</translation>
    </message>
    <message>
        <source>Save image after snap</source>
        <translation type="vanished">Bild nach Aufnahme speichern</translation>
    </message>
    <message>
        <source>Data type</source>
        <translation type="vanished">Dateityp</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Change Folder...</source>
        <translation>Verzeichnis wechseln...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Data Type</source>
        <translation>Dateityp</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Options...</source>
        <translation>Optionen...</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Add Timestamp</source>
        <translation>Zeitstempel hinzufügen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Tag of DataObject</source>
        <translation>... zum Tag des DataObjects</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add to File Name</source>
        <translation>... zum Dateinamen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Acquire</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Multishot</source>
        <translation>Mehrfachaufnahme</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Multiple Snapshots</source>
        <translation>Mehrere Aufnahmen</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Auto Grabbing</source>
        <translation>Auto-Grabbing</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>File Name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <source>Multiple snapshots</source>
        <translation type="vanished">Mehrere Aufnahmen</translation>
    </message>
    <message>
        <location line="-92"/>
        <source>single</source>
        <translation>Einzel</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>stack</source>
        <translation>Stapel</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Timer</source>
        <translation></translation>
    </message>
    <message>
        <source>Auto grabbing</source>
        <translation type="vanished">Live-Anzeige</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Snapshot</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%p%</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>progress...</source>
        <translation>Fortschritt...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>pic_</source>
        <translation>bild_</translation>
    </message>
    <message>
        <source>File name</source>
        <translation type="vanished">Dateiname</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>DialogTimerManager</name>
    <message>
        <location filename="../ui/dialogTimerManager.ui" line="+14"/>
        <source>Timer Manager</source>
        <translation>Timer-Manager</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>List of all Timers:</source>
        <translation>Liste aller Timer:</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>stop all</source>
        <translation>Alles stoppen</translation>
    </message>
</context>
<context>
    <name>DialogVariableDetail</name>
    <message>
        <location filename="../ui/dialogVariableDetail.ui" line="+17"/>
        <source>Variable Detail</source>
        <translation>Variablendetail</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Value:</source>
        <translation>Wert:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy name to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
</context>
<context>
    <name>HelpTreeDockWidget</name>
    <message>
        <location filename="../ui/helpTreeDockWidget.ui" line="+26"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>CommandLinkButton</source>
        <translation></translation>
    </message>
    <message>
        <location line="-55"/>
        <source>&lt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Help database is loading...</source>
        <translation>Hilfedatenbank wird geladen...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../python/pythonPlotItem.cpp" line="+236"/>
        <source>data object cannot be converted to a shared data object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>pick points operation interrupted by user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+105"/>
        <source>draw points operation interrupted by user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../python/pythonShape.cpp" line="+194"/>
        <location line="+66"/>
        <source>param2 must be a double value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+919"/>
        <source>point1 cannot be changed for square and circle. Change center and width / height.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This type of shape has no &apos;point1&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+72"/>
        <source>point2 cannot be changed for square and circle. Change center and width / height.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This type of shape has no &apos;point2&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+90"/>
        <source>This type of shape has no &apos;center&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+982"/>
        <source>%s missing</source>
        <translation type="unfinished">%s fehlt</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>%s: float64 array with two elements required (x,y)</source>
        <translation type="unfinished">%s: Float64-Array mit zwei elementen erwartet (x,y)</translation>
    </message>
    <message>
        <location filename="../python/pythonUi.cpp" line="+3380"/>
        <source>no widget name specified</source>
        <translation type="unfinished">Kein Widget-Name vergeben</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+192"/>
        <source>no addin-manager found</source>
        <translation type="unfinished">Der &apos;AddInManager&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-184"/>
        <source>the first parameter must contain the widget name as string</source>
        <translation type="unfinished">Der erste Parameter muss einen String mit dem Widget-Name enthalten</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+184"/>
        <source>Could not find plugin widget with name &apos;%1&apos;</source>
        <translation type="unfinished">Das Plugin-Widget mit dem Namen &apos;%1&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-177"/>
        <location line="+184"/>
        <source>Could not get parameters for plugin widget &apos;%1&apos;</source>
        <translation type="unfinished">Vom Plugin-Widget &apos;%1&apos; konnten keine Parameter gelesen werden</translation>
    </message>
    <message>
        <location filename="../python/pythontParamConversion.cpp" line="+329"/>
        <source>Type of ParamBase could not be guessed with given PyObject.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+159"/>
        <source>Given paramBaseType is unsupported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error while converting value from PyObject to ParamBase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../python/pythonPlugins.cpp" line="+297"/>
        <source>plugin has no configuration dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>timeout while showing toolbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+38"/>
        <source>Member &apos;showDockWidget&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-9"/>
        <source>timeout while hiding toolbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+239"/>
        <source>timeout while getting name parameter</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen des Parameternamens</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+225"/>
        <source>Member &apos;getParam&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-196"/>
        <source>you must provide at least one parameter with the name of the function</source>
        <translation type="unfinished">Es muss mindestens ein Parameter für dem Funktionsnamen unterstützt werden</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>the first function name parameter can not be interpreted as string</source>
        <translation type="unfinished">Der erste Parameter sollte der Funktionsname sein, kann jedoch nicht als String erkannt werden</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>plugin does not provide an execution of function &apos;%s&apos;</source>
        <translation type="unfinished">Das Plugin unterstützt nicht die Ausführung der Funktion &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>timeout while calling specific function in plugin.</source>
        <translation type="unfinished">Zeitüberschreitung beim Aufruf spezifischer Funktionen im Plugin.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;execFunc&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+125"/>
        <source>timeout while getting parameter</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen der Parameter</translation>
    </message>
    <message>
        <location line="+344"/>
        <source>timeout.</source>
        <translation type="unfinished">Zeitüberschreitung.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;setParam&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Member &apos;closeAddIn&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+174"/>
        <location line="+1785"/>
        <source>Member &apos;initAddIn&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1527"/>
        <source>timeout while calibration</source>
        <translation type="unfinished">Zeitüberschreitung bei der Kalibrierung</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;calib&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+104"/>
        <source>timeout while setting origin</source>
        <translation type="unfinished">Zeitüberschreitung beim Setzen auf den Nullpunkt</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Member &apos;setOrigin&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>timeout while getting Status</source>
        <translation type="obsolete">Zeitüberschreitung beim Lesen des Status</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+39"/>
        <source>timeout while getting status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-26"/>
        <location line="+39"/>
        <source>Member &apos;getStatus&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+116"/>
        <source>timeout while getting position values</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen der Positionswerte</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;getPos&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+508"/>
        <source>timeout while setting absolute position</source>
        <translation type="unfinished">Zeitüberschreitung beim setzen der absoluten Position</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;setPosAbs&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+74"/>
        <source>timeout while setting relative position</source>
        <translation type="unfinished">Zeitüberschreitung beim Setzten der relativen Position</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;setPosRel&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Itom ActuatorPlugin type in python</source>
        <translation type="unfinished">Itom ActuatorPlugin-Typ in Python</translation>
    </message>
    <message>
        <location line="+561"/>
        <source>timeout while calling &apos;startDevice&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;startDevice&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+41"/>
        <source>timeout while stopping device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Member &apos;stopDevice&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>timeout while calling &apos;acquire&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;acquire&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+134"/>
        <source>timeout while calling &apos;getVal&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+104"/>
        <location line="+43"/>
        <source>timeout while calling &apos;copyVal&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-31"/>
        <location line="+41"/>
        <source>Member &apos;copyVal&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>copyVal function only implemented for typeADDA and typeGrabber</source>
        <translation type="unfinished">Die Funktion &apos;copyVal&apos; wurde nur für ADDA-Wandler und Grabber implementiert</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+106"/>
        <source>timeout while calling &apos;setVal&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-94"/>
        <location line="+106"/>
        <source>Member &apos;setVal&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>timeout while calling &apos;enableAutoGrabbing&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Member &apos;enableAutoGrabbing&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>timeout while calling &apos;disableAutoGrabbing&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Member &apos;disableAutoGrabbing&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>timeout while calling &apos;enable/disableAutoGrabbing&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Member &apos;enableAutoGrabbing&apos; or &apos;disableAutoGrabbing&apos; of plugin could not be invoked (error in signal/slot connection).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+91"/>
        <source>timeout while setting the current &apos;autoGrabbingInterval&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+47"/>
        <source>timeout while obtaining the current &apos;autoGrabbingInterval&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+369"/>
        <source>Itom DataIOPlugin type in python</source>
        <translation type="unfinished">Itom DataIOPlugin-Typ in Python</translation>
    </message>
    <message>
        <location filename="../python/pythonPCL.cpp" line="+1862"/>
        <location line="+111"/>
        <location line="+2027"/>
        <location line="+88"/>
        <source>api function pointer not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2222"/>
        <source>No more memory available when saving point cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The exception &apos;%s&apos; has been thrown when saving a point cloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+5"/>
        <source>An unspecified exception has been thrown when saving a point cloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Temporary file for writing point cloud binary data could not be opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Temporary file for writing point cloud binary data could not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>temporary file could not be opened (II)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>No more memory available when loading point cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The exception &apos;%s&apos; has been thrown when loading a point cloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+5"/>
        <source>An unspecified exception has been thrown when loading a point cloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The pickled data must be a byte array for establishing the pointCloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../python/pythonItom.cpp" line="+2845"/>
        <source>Button must have a valid name.</source>
        <translation type="unfinished">Der Button muss einen gültigen Namen haben.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Menu element must have a valid key.</source>
        <translation type="unfinished">Menüelement muss einen gültigen Schlüssel haben.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>For menu elements of type &apos;BUTTON&apos; any type of code (String or callable method or function) must be indicated.</source>
        <translation type="unfinished">Für Menüelemente vom Typ &apos;button&apos; muss jede Art von Code (Sting oder Funktionen/Methoden) erkennbar sein.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>A menu element of type &apos;separator&apos; can not execute some code. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;separator&apos; kann keinen Code ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A menu element of type &apos;separator&apos; can not execute any function or method. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;separator&apos; kann keine Funktionen oder Methoden ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>A menu element of type &apos;menu&apos; can not execute some code. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;menu&apos; kann keinen Code ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A menu element of type &apos;menu&apos; can not execute any function or method. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;menu&apos; kann keine Funktionen oder Methoden ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+887"/>
        <source>Error while parsing parameters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>OpenCV Error: %s (%s) in %s, file %s, line %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown function</source>
        <translation type="unfinished">Unbekannte Funktion</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The exception &apos;%s&apos; has been thrown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The exception &apos;&lt;unknown&gt;&apos; has been thrown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>An unspecified exception has been thrown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1286"/>
        <source>No code is given</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Given argument must be of type tuple or list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Proxy object of function or method could not be created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Code is no function or method call and no executable code string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+35"/>
        <location line="+32"/>
        <source>Python engine is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../python/pythonCommon.cpp" line="+81"/>
        <source>Value could not be converted to integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Value could not be converted to double</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Value could not be converted to complex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Error while converting python object to string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Unknown parameter type</source>
        <translation type="unfinished">Unbekannter Parametertyp</translation>
    </message>
    <message>
        <location line="+660"/>
        <source>Wrong number of parameters. Mandatory parameters are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Optional parameters are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+116"/>
        <source>Wrong number of parameters (%i given, %i mandatory and %i optional required)</source>
        <translation type="unfinished">Falsche Anzahl an Parametern (%i übergeben, %i Pflichtparameter und %i optionale Parameter erforderlich)</translation>
    </message>
    <message>
        <location line="-92"/>
        <source>Parameter %d - %s passed as arg and keyword!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Optional parameter %d - %s passed as arg and keyword!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>There are keyword arguments that does not exist in mandatory or optional parameters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Wrong number of parameters
 Mandatory parameters are:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+30"/>
        <location line="+30"/>
        <location line="+36"/>
        <source>Wrong parameter type</source>
        <translation type="unfinished">Falscher Parametertyp</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>paramVecIn is NULL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Keyword autoLoadParams not of integer type</source>
        <translation type="unfinished">Schlüsselwort autoLoadParams ist nicht vom Typ Integer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Could not delete Keyword: autoLoadParams</source>
        <translation type="unfinished">Schlüsselwort kann nicht gelöscht werden: autoLoadParams</translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+13"/>
        <source>Unknown parameter of type QVariant</source>
        <translation type="unfinished">Unbekannter Parameter vom Typ QVariant</translation>
    </message>
    <message>
        <location line="+459"/>
        <source>- Unknown message -</source>
        <translation type="unfinished">- Unbekannte Nachricht -</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>%s with message: 
%s</source>
        <translation type="unfinished">%s mit Meldung: 
%s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s with unspecified error.</source>
        <translation type="unfinished">%s mit nicht spezifiziertem Fehler.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Could not load plugin %s with error message: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not load plugin %s with unspecified error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error executing function %s with error message: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error executing function %s with unspecified error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error invoking function %s with error message: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified error invoking function %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error while getting property info %s with error message: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified error while getting property info %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error invoking exec-function %s with error message: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error invoking exec-function %s with unspecified error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Warning while %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s with unspecified warning.</source>
        <translation type="unfinished">%s mit nicht spezifizierter Warnung.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning while loading plugin %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified warning while loading plugin %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning while executing function %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified warning while executing function %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning while invoking function %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified warning while invoking function %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning while getting property info %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified warning while getting property info %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning while invoking exec-function %s: 
%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unspecified warning invoking exec-function %s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../organizer/helpSystem.cpp" line="+314"/>
        <source>File could not be opened.</source>
        <translation type="unfinished">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Load XML file failed: file seems corrupt</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Datei scheint defekt zu sein</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Load XML file failed:  wrong xml version</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Falsche XML-Version</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Load XML file failed: wrong document encoding</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Falscher Dokumenten-Encoder</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Load XML file failed: could not intepret checksum content as uint</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Checksummenfehler</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Collection project file could not be opened</source>
        <translation type="unfinished">Die &apos;Collection&apos;-Projektdatei konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Error calling qcollectiongenerator</source>
        <translation type="unfinished">Fehler beim Aufruf von QCollectiongenerator</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Templates for plugin documentation not found. Directory &apos;docs/pluginDoc/template&apos; not available. Plugin documentation will not be built.</source>
        <translation type="unfinished">Die Templates für die Plugin-Dokumentation wurden nicht gefunden. Das Verzeichnis &apos;docs/pluginDoc/template&apos; ist nicht verfügbar. Die Plugin-Dokumentation wird nicht erstellt.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Folder &apos;build&apos; as subfolder of &apos;docs/pluginDoc&apos; could not be created. Plugin documentation will not be built.</source>
        <translation type="unfinished">Der Ordner &apos;build&apos; als Unterverzeichnis von &apos;docs/pluginDoc&apos; konnte nicht erstellt werden. Die Plugin-Dokumentation wird nicht erstellt.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not clear folder &apos;docs/pluginDoc/build&apos;. Plugin documentation will not be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>No plugin directory available. No plugin documentation will be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Error opening index.html of template folder</source>
        <translation type="unfinished">Fehler beim Öffnen von index.html aus dem Template-Ordner</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error opening itomPluginDoc.qhp of template folder</source>
        <translation type="unfinished">Fehler beim Öffnen von itomPluginDoc.qhp aus dem Template-Ordner</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Error writing index.html of template folder</source>
        <translation type="unfinished">Fehler beim Schreiben von index.html in den Template-Ordner</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error writing itomPluginDoc.qhp of template folder</source>
        <translation type="unfinished">Fehler beim Schreiben von itomPluginDoc.qhp in den Template-Ordner</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Could not copy folder &apos;docs/pluginDoc/template/_static&apos; to &apos;docs/pluginDoc/build/_static&apos;</source>
        <translation type="unfinished">Das Verzeichnis &apos;docs/pluginDoc/template/_static&apos; kann nicht nach &apos;docs/pluginDoc/build/_static&apos; kopiert werden</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Error calling qhelpgenerator for creating the plugin documentation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../organizer/designerWidgetOrganizer.cpp" line="+199"/>
        <source>Unable to load translation file &apos;%1&apos;. Translation file is empty.</source>
        <translation type="unfinished">Die Übersetzungsdatei &apos;%1&apos; kann nicht geladen werden. Diese Datei ist leer.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to find translation file.</source>
        <translation type="unfinished">Übersetzungsdatei wurde nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../helper/qpropertyHelper.cpp" line="+60"/>
        <source>Transformation error to PointF: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to PointF: 2 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Transformation error to Point: at least one value could not be transformed to integer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Point: 2 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Transformation error to Rect: at least one value could not be transformed to integer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Rect: 4 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Transformation error to RectF: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to RectF: 4 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Transformation error to Vector2D: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Vector2D: 2 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Transformation error to Vector3D: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Vector3D: 3 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Transformation error to Vector4D: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Vector4D: 4 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Transformation error to Size: at least one value could not be transformed to integer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to Size: 2 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Transformation error to AutoInterval: at least one value could not be transformed to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Transformation error to AutoInterval: 2 values required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transformation error to vector of shapes: at least one item could not be interpreted as shape.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Transformation error to QVector&lt;int&gt;: at least one value could not be transformed to int.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Transformation error to AutoInterval: value must be [min,max] or &apos;auto&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+111"/>
        <location line="+9"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No conversion from QVariant type &apos;%s&apos; to &apos;%s&apos; is possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>The value %i contains a bitmask that is not fully covered by an or-combination of the enumeration %s::%s (flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The value %i does not exist in the enumeration %s::%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The key %s does not exist in the enumeration %s::%s (flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The key %s does not exist in the enumeration %s::%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Use an integer or a string for a value of the enumeration %s::%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Property &apos;%s&apos; could not be read</source>
        <translation type="unfinished">Die Eigenschaft &apos;%s&apos; kann nicht gelesenwerden</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+53"/>
        <source>Invalid object</source>
        <translation type="unfinished">Ungültiges Objekt</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>Property &apos;%s&apos; is not writeable.</source>
        <translation type="unfinished">Die Eigenschaft &apos;%s&apos; ist nicht beschreibbar.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Property &apos;%s&apos; could not be set. Maybe wrong input type.</source>
        <translation type="unfinished">Die Eigenschaft &apos;%s&apos; kann nicht gesetzt werden. Evtl. ein falscher Input-Typ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Property &apos;%s&apos; does not exist.</source>
        <translation type="unfinished">Die Eigenschaft &apos;%s&apos; existiert nicht.</translation>
    </message>
    <message>
        <location filename="../helper/versionHelper.cpp" line="+81"/>
        <source>none</source>
        <translation type="unfinished">nicht</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>The version contains locally changed code! </source>
        <translation type="unfinished">Diese Version beinhaltet lokal geänderten Code! </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The version contains unversioned files (e.g. from __pycache__-files)!</source>
        <translation type="unfinished">Die Version enthält nicht versionierte Dateien (z. B. von __pycache__-files)!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Build from a clean version.
</source>
        <translation type="unfinished">Das &apos;Build&apos; ist keine &apos;clean version&apos; von GIT.
</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This version of itom is not under version control (no GIT or SVN)!
</source>
        <translation type="unfinished">Diese Version von itom ist nicht unter der Versionskontrolle (kein GIT oder SVN)!
</translation>
    </message>
    <message>
        <location filename="../helper/IOHelper.cpp" line="+70"/>
        <source>Itom Files</source>
        <translation type="unfinished">ITOM-Dateien</translation>
    </message>
    <message>
        <location filename="../api/apiFunctionsGraph.cpp" line="+105"/>
        <location line="+33"/>
        <source>Color map &apos;%s&apos; not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+67"/>
        <source>DesignerWidgetOrganizer is not available</source>
        <translation type="unfinished">&apos;designerWidgetOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+269"/>
        <location line="+28"/>
        <source>uiOrganizer is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-224"/>
        <source>Timeout while unregistering live image from camera.</source>
        <translation type="unfinished">Zeitüberschreitung beim Stoppen des Livebilds der Kamera.</translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+25"/>
        <source>LiveDataSource is no instance of ito::AddInDataIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+25"/>
        <source>LiveDataSource or liveDataView are NULL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>FigureClass is NULL. No settings could be retrieved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>FigureClass is not inherited from AbstractFigure. No settings could be retrieved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Timeout while loading plugin widget</source>
        <translation type="unfinished">Zeitüberschreitung beim Laden der Plugin-Widgets</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error retrieving widget pointer</source>
        <translation type="unfinished">Fehler bei der Abfrage des Widget-Pointers</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error closing dialog</source>
        <translation type="unfinished">Fehler beim Schließen des Dialogs</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Timeout showing dialog</source>
        <translation type="unfinished">Zeitüberschreitung beim Anzeigen des Dialogs</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>UI-Organizer is not available!</source>
        <translation type="unfinished">&apos;UI-Organizer&apos; ist nicht verfügbar!</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Plot widget does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Timeout while sending variables to python workspace. Python is maybe busy. Try it later again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Python is not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../organizer/widgetWrapper.cpp" line="+315"/>
        <source>QListWidget object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+162"/>
        <source>ComboBox object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>QTabWidget object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>QMainWindow object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>QWidget object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>QTableWidget object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not access row / col, maybe out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+20"/>
        <source>QTableView object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>QStatusBar object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>QToolBar object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>QAction object is null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Slot or widget not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codeEditor/textBlockUserData.h" line="+105"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation type="unfinished">Warnung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <source>Paper color</source>
        <translation type="obsolete">Papierfarbe</translation>
    </message>
    <message>
        <location filename="../codeEditor/syntaxHighlighter/codeEditorStyle.cpp" line="+57"/>
        <source>Current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Keyword Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Builtin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentieren</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Docstring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Number</source>
        <translation type="unfinished">Anzahl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Class Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Stream Output (Command Line)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Stream Error (Command Line)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Self</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Decorator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Function</source>
        <translation type="unfinished">Funktion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Operator Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/scriptEditorPrinter.cpp" line="+62"/>
        <source>Page %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WidgetFindWord</name>
    <message>
        <location filename="../ui/widgetFindWord.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>find:</source>
        <translation>Suche:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>type search text...</source>
        <translation>Suchtext...</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>up</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shift+F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>down</source>
        <translation>Abwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>mark all</source>
        <translation>Alles markieren</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>whole word</source>
        <translation>Ganzes Wort</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>match case</source>
        <translation>Groß/Klein</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>wrap around</source>
        <translation>Vom Ende zum Anfang</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>reg. expr.</source>
        <translation>Reg. Ausdr.</translation>
    </message>
</context>
<context>
    <name>WidgetInfoBox</name>
    <message>
        <location filename="../ui/widgetInfoBox.ui" line="+14"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleGeneral</name>
    <message>
        <location filename="../ui/widgetPropConsoleGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Copy text from console</source>
        <translation>Text aus der Konsole in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>If a command is copied from the command line, the &apos;&gt;&gt;&apos; characters (representing the start of a command) should be ignored. If so check the following option to let itom remove this characters before copying it to the clipboard.</source>
        <translation>Wird Text aus der Konsole kopiert, sollten die &apos;&gt;&gt;&apos;-Zeichen, die den Start der Kommandozeile representieren, ignoriert werden. Um diese vor dem Kopieren automatisch von itom löschen zu lassen folgende Option auswählen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Modify text from console before copying to clipboard. </source>
        <translation>Text vor dem Kopieren aus der Konsole modifizieren. </translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Paste text to console</source>
        <translation>Text aus der Zwischenablage in die Konsole einfügen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>If subparts of a code are copied from any script and pasted to the console, it might happen that the whole code block is already globally indentend. The code can then not be executed. Check the following option to let itom remove the global indentation level before pasting it to the console.</source>
        <translation>Wenn Teile eines Python-Codes in die Konsole eingefügt werden sollen, kann es vorkommen, dass der einzufügende Code-Block bereits eingerückt ist und dadurch nicht ausgeführt werden kann. Um den Code-Block von itom automatisch anpassen zu lassen folgende Option auswählen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Modify text in clipboard before pasting to console. </source>
        <translation>Text vor dem Einfügen in die Konsole modifizieren. </translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleLastCommand</name>
    <message>
        <location filename="../ui/widgetPropConsoleLastCommand.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Command history enabled (set disable to clear the history!)</source>
        <translation>Befehlsliste aktivieren (Deaktivieren löscht die Liste!)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Number of saving commands</source>
        <translation>Anzahl zu speichernde Befehle</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Do not add if the new command is equal to the last one</source>
        <translation>Nicht hinzufügen, wenn der letzte Befehl in der Liste identisch ist</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Date color</source>
        <translation>Farbe der Datumsanzeige</translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleWrap</name>
    <message>
        <location filename="../ui/widgetPropConsoleWrap.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Line Wrap</source>
        <translation type="vanished">Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Line Wrap (based on width of console widget)</source>
        <translation type="unfinished">Zeilenumbruch (basierend auf der Breite der Konsole)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Lines are not wrapped</source>
        <translation>Zeile nicht umbrechen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Lines are wrapped at word boundaries</source>
        <translation>Wortweiser Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Lines are wrapped at character boundaries</source>
        <translation>Zeichenweiser Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Visual Appearance</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Split Long Lines</source>
        <translation type="unfinished">Umbrechung langer Zeilen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>For performance reasons, it is recommended to split very long lines of text into several lines.</source>
        <translation type="unfinished">Aus Gründen der Performance ist es ratsam sehr lange Textzeilen in mehrere Zeilen zu splitten.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Maximum line length:</source>
        <translation type="unfinished">Maximale Zeilenlänge:</translation>
    </message>
    <message>
        <source>Flag at start of every wrapped line</source>
        <translation type="vanished">Markierung zu Beginn jeder umgebrochenen Zeile</translation>
    </message>
    <message>
        <source>No wrap flag</source>
        <translation type="vanished">Keine Markierung</translation>
    </message>
    <message>
        <source>Wrap flag displayed by the text</source>
        <translation type="vanished">Markierung im Text</translation>
    </message>
    <message>
        <source>Wrap flag displayed by the border</source>
        <translation type="vanished">Markierung am Rand</translation>
    </message>
    <message>
        <source>Flag at end of every wrapped line</source>
        <translation type="vanished">Markierung am Ende jeder umgebrochenen Zeile</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Number of characters a wrapped line is indented by</source>
        <translation>Anzahl Zeichen beim Einrücken einer umbegrochenen Zeile</translation>
    </message>
    <message>
        <source>Wrap Indentation Mode</source>
        <translation type="vanished">Einrückmodus beim Zeilenumbruch</translation>
    </message>
    <message>
        <source>Wrapped sub-lines are indented by the amount indicated above</source>
        <translation type="vanished">Umgebrochene Unterzeile mit oben angegebenem Wert einrücken</translation>
    </message>
    <message>
        <source>Wrapped sub-lines are indented by the same amount as the first sub-line</source>
        <translation type="vanished">Folgende Unterzeilen wie die erste Unterzeile einrücken</translation>
    </message>
    <message>
        <source>Wrapped sub-lines are indented by the same amount as the first sub-line plus one mor level of indentation</source>
        <translation type="vanished">Jede Unterzeilen um einen Einzug mehr einrücken als die Vorherige</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorAPI</name>
    <message>
        <source>Add API</source>
        <translation type="vanished">Hinzufügen</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Entfernen</translation>
    </message>
    <message>
        <source>Base path: </source>
        <translation type="vanished">Stammverzeichnis: </translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorAutoCompletion</name>
    <message>
        <location filename="../ui/widgetPropEditorAutoCompletion.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Auto-completion enabled (requires Python packages jedi and parso)</source>
        <translation type="unfinished">Autovervollständigung einschalten (hierfür sind die Python-Pakete jedi und parso notwendig)</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Filter Mode</source>
        <translation type="unfinished">Filtermodus</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Completion is based on prefix only - fast</source>
        <translation type="unfinished">Vervollständigung nur basierend auf gleichem Prefix - schnell</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Completion checks if prefix is contained in the suggestions - fast</source>
        <translation type="unfinished">Vervollständigung prüft Prefix als Teil des Vorschlags - schnell</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fuzzy filtering - most powerful method - slow</source>
        <translation type="unfinished">Fuzzy Filtering - sehr mächtige Methode - langsam</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Case sensitive</source>
        <translation>Groß-/Kleinschreibung berücksichtigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show tooltips</source>
        <translation type="unfinished">Tooltips anzeigen</translation>
    </message>
    <message>
        <source>Use fill-up characters</source>
        <translation type="vanished">Füllzeichen benutzen</translation>
    </message>
    <message>
        <source>Auto-completion enabled</source>
        <translation type="vanished">Autovervollständigung einschalten</translation>
    </message>
    <message>
        <location line="-85"/>
        <source>Threshold</source>
        <translation>Grenzwert</translation>
    </message>
    <message>
        <source>Replace word right of the cursor if entry from list is selected</source>
        <translation type="vanished">Ersetze Wörter hinter dem Cursor wenn der Eintrag von der Liste ausgewählt wird</translation>
    </message>
    <message>
        <source>Show single</source>
        <translation type="vanished">Einzeln anzeigen</translation>
    </message>
    <message>
        <source>Sources for auto-completion</source>
        <translation type="vanished">Quellen der Autovervollständigung</translation>
    </message>
    <message>
        <source>Use all available sources</source>
        <translation type="vanished">Alle verfügbaren Quellen nutzen</translation>
    </message>
    <message>
        <source>Use text in the current document as source</source>
        <translation type="vanished">Den Text im aktuellen Dokument als Quelle nutzen</translation>
    </message>
    <message>
        <source>Use any installed APIs as source</source>
        <translation type="vanished">Jede installierte API als Quelle nutzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorCalltips</name>
    <message>
        <location filename="../ui/widgetPropEditorCalltips.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Calltips enabled (requires Python packages jedi and parso)</source>
        <translation type="unfinished">Vorschläge aktivieren (hierfür sind die Python-Pakete jedi und parso notwendig)</translation>
    </message>
    <message>
        <source>Calltips enabled</source>
        <translation type="vanished">Vorschläge aktivieren</translation>
    </message>
    <message>
        <source>Number of calltips</source>
        <translation type="vanished">Anzahl der Vorschläge</translation>
    </message>
    <message>
        <source>Context display options</source>
        <translation type="vanished">Optionen der Kontextanzeige</translation>
    </message>
    <message>
        <source>Don&apos;t show context information</source>
        <translation type="vanished">Kontextinformationen nicht anzeigen</translation>
    </message>
    <message>
        <source>Show context information, if no prior autocompletion</source>
        <translation type="vanished">Nur Kontextinformationen anzeigen wenn die Autovervollständigung nichts liefert</translation>
    </message>
    <message>
        <source>Show context information</source>
        <translation type="vanished">Kontextinformationen immer anzeigen</translation>
    </message>
    <message>
        <source>A context is any scope (e.g. Python module) prior to the function/method name</source>
        <translation type="vanished">Ein Kontext ist jeder Bereich (z. B. Python-Modul) vor dem Namen der Funktion/Methode</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorGeneral</name>
    <message>
        <location filename="../ui/widgetPropEditorGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Indentation</source>
        <translation>Einzug</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Auto Indentation</source>
        <translation>Automatischer Einzug</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Indentation Guides</source>
        <translation>Einzugsmarker anzeigen</translation>
    </message>
    <message>
        <source>Use tabs for indentation</source>
        <translation type="vanished">Tabs als Einzug verwenden</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show Whitespace</source>
        <translation>Leerzeichen anzeigen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Indentation Width</source>
        <translation>Einzugsgröße</translation>
    </message>
    <message>
        <source>Indentation Warning</source>
        <translation type="vanished">Warnungen beim Einrücken</translation>
    </message>
    <message>
        <source>Indentation is displayed as being bad, if...</source>
        <translation type="vanished">Warnen wenn...</translation>
    </message>
    <message>
        <source>No warning</source>
        <translation type="vanished">Keine Warnung</translation>
    </message>
    <message>
        <source>Indentation is made up of a different combination (tabs/spaces) compared to previous line</source>
        <translation type="vanished">Der Einzug zweier aufeinanderfolgender Zeilen unterschiedlich erstellt wurden (Tabs/Leerzeichen)</translation>
    </message>
    <message>
        <source>Indentation is made up of spaces followed by tabs</source>
        <translation type="vanished">Der Einzug aus einer Kombination von Leerzeichen und Tabs besteht</translation>
    </message>
    <message>
        <source>Indentation contains spaces</source>
        <translation type="vanished">Der Einzug Leerzeichen enthält</translation>
    </message>
    <message>
        <source>Indentation contains tabs</source>
        <translation type="vanished">Der Einzug Tabs enthält</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Extra ascent (above each line)</source>
        <translation>Aufsteigend (über jeder Zeile)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Extra descent (below)</source>
        <translation>Absteigend (unter jeder Zeile)</translation>
    </message>
    <message>
        <location line="-76"/>
        <source>End-of-line (EOL) mode</source>
        <translation>Modus für Zeilenende</translation>
    </message>
    <message>
        <location line="-50"/>
        <source>Use Tabs for Indentation</source>
        <translation>Tabs als Einzug verwenden</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Windows</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unix</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Line Spacing</source>
        <translation>Zeilenabstand</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+30"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-75"/>
        <source>Mac</source>
        <translation></translation>
    </message>
    <message>
        <source>Folding Style</source>
        <translation type="vanished">Gliederungsanzeige</translation>
    </message>
    <message>
        <source>Squares</source>
        <translation type="vanished">Quadrate</translation>
    </message>
    <message>
        <source>Circles</source>
        <translation type="vanished">Kreise</translation>
    </message>
    <message>
        <source>Squares + Tree</source>
        <translation type="vanished">Quadrate + Baum</translation>
    </message>
    <message>
        <source>Circles + Tree</source>
        <translation type="vanished">Kreise + Baum</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Nichts</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorGotoAssignment</name>
    <message>
        <location filename="../ui/widgetPropEditorGotoAssignment.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Go to definition and assignment enabled (requires Python packages jedi and parso)</source>
        <translation type="unfinished">Gehe zu Definition und Zuweisung aktivieren (hierfür sind die Python-Pakete jedi und parso notwendig)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Check &quot;goto definition&quot; if Ctrl-key is pressed and mouse is moved over word</source>
        <translation type="unfinished">Prüfe &quot;gehe zu Definition&quot;, wenn bei gedrückter Strg-Taste die Maus über ein Wort bewegt wird (falls eine Zurückverfolgung möglich ist, wird das Wort unterstrichen und kann angeklickt werden)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Which &quot;goto mode&quot; should be checked, if mouse is moved over word:</source>
        <translation type="unfinished">Welcher &quot;gehe zu&quot;-Modus soll aktiv sein, wenn die Maus über ein Wort bewegt wird:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Goto definition (returns the end, not the first definition)</source>
        <translation type="unfinished">Gehe zu Definition (gibt die Unterste, nicht die erste Definition zurück)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Goto assignment (returns the first definition)</source>
        <translation type="unfinished">Gehe zu Zuweisung (gibt die erste Definition zurück)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Goto assignment and follow imports (the goto call will also follow imports)</source>
        <translation type="unfinished">Gehe zu Zuweisung inkl. importierter Module und Pakete</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorScripts</name>
    <message>
        <location filename="../ui/widgetPropEditorScripts.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <source>Python Syntax-Checker (uses Python-Module &quot;frosted&quot;)</source>
        <translation type="vanished">Python-Syntaxprüfung (mit Python-Modul &quot;frosted&quot;)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Automatically include itom module for syntax checker</source>
        <translation>itom-Modul für die Syntaxprüfung automatisch hinzufügen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Check interval:</source>
        <extracomment>The timer is started when entering a new line</extracomment>
        <translation>Prüfintervall:</translation>
    </message>
    <message>
        <source>[sec]</source>
        <translation type="vanished">[s]</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Python Syntax-Checker (uses Python-Module &quot;pyflakes&quot; or &quot;frosted&quot;)</source>
        <translation>Python-Syntaxprüfung (mit Python-Modul &quot;pyflakes&quot; oder &quot;frosted&quot;)</translation>
    </message>
    <message>
        <location line="+54"/>
        <location line="+67"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Class Navigator</source>
        <translation>Klassennavigator</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>This feature enables two comboboxes with class- and method-navigation above the editor.</source>
        <translation>Diese Einstellung aktiviert über dem Editor zwei Comboboxen für die Navigation in den Klassen und Methoden.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Activate timer</source>
        <translation>Timer aktivieren</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Edge Mode</source>
        <translation>Zeilenbegrenzung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Long lines are not marked in the script editor (no edge mode)</source>
        <translation>Zeile ohne Begrenzungsmarkierung</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A vertical line is drawn at a given column position (recommended for monospace fonts)</source>
        <translation>Eine vertikale Linie an gegebener Spaltenposition (empfohlen für monospace Schriftarten)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set the background color of characters after the column limit to a specific color</source>
        <translation>Die Zeichen ab der angegebenen Spalte mit vorgegebener Hintergrundsfarbe darstellen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edge column:</source>
        <translation>Begrenzungsspalte:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Background color:</source>
        <translation>Hintergrundfarbe:</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorStyles</name>
    <message>
        <location filename="../ui/widgetPropEditorStyles.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Foreground Color</source>
        <translation>Vordergundsfarbe</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Background Color</source>
        <translation>Hintergrundsfarbe</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <source>Fill to end of line</source>
        <translation type="vanished">Bis Zeilenende ausfüllen</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>Reset All To Default</source>
        <translation>Standard zurücksetzen</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Import Style...</source>
        <translation>Style importieren...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Export Style...</source>
        <translation>Style exportieren...</translation>
    </message>
    <message>
        <location line="-228"/>
        <source>Sample Text</source>
        <translation>Beispieltext</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Style Settings</source>
        <translation>Einstellungen der Ansicht</translation>
    </message>
    <message>
        <source>Foreground color</source>
        <translation type="vanished">Vordergrundfarbe</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation type="vanished">Hintergrundfarbe</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Highlight current line</source>
        <translation>Aktuelle Zeile hervorheben</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Overall Font Size</source>
        <translation>Generelle Schriftgröße</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>General Settings</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Set All Text Backgrounds
To Transparent</source>
        <translation type="unfinished">Alle Texthintergründe
transparent setzen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Import / Export</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WidgetPropFigurePlugins</name>
    <message>
        <location filename="../ui/widgetPropFigurePlugins.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Installed Figure and Plot Plugins</source>
        <translation>Installierte Figures und Plugins</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Default Figures</source>
        <translation>Standard Figures</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Reset To Standard</source>
        <translation>Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralApplication</name>
    <message>
        <location filename="../ui/widgetPropGeneralApplication.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show message before closing the application</source>
        <translation>Vor dem Schließen von itom fragen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Timeouts</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>General timeout</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+36"/>
        <location line="+36"/>
        <source>-1 (infinity)</source>
        <translation>-1 (endlos)</translation>
    </message>
    <message>
        <location line="-69"/>
        <location line="+36"/>
        <location line="+36"/>
        <source> ms</source>
        <translation></translation>
    </message>
    <message>
        <location line="-53"/>
        <location line="+36"/>
        <location line="+36"/>
        <source>(0: no, -1: infinite timeout)</source>
        <translation>(0: aus, -1: endlos)</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>Plugin init and close timeout</source>
        <translation>Plugins: Initialisieren und Schließen</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>File load and save timeout</source>
        <translation>Dateien: Speichern und Öffnen</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Library Path(s)</source>
        <translation>Bibliotheksverzeichnis(se)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The following paths are prepended or appended to the PATH environment variable used by itom. It is modified before itom starts loading plugins or designer plugins. These paths do not affect the overall PATH environment variable of the operating system. Change the order of the paths to decide if they are prepended or appended to the overall PATH environment variable.</source>
        <translation>Die folgenden Verzeichnisse werden beim Laden der Plugins und Designer-Plugins als Bibliotheksquellen in der gegebenen Reihenfolge berücksichtigt.</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Move Up</source>
        <translation>Nach Oben</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Move Down</source>
        <translation>Nach Unten</translation>
    </message>
    <message>
        <location line="-37"/>
        <source>Add Path</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralLanguage</name>
    <message>
        <location filename="../ui/widgetPropGeneralLanguage.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current Language:</source>
        <translation>Aktuell eingestellte Sprache:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Affected after program restart!</source>
        <translation>Einstellung wird erst nach einem Programmneustart wirksam!</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralPlotSettings</name>
    <message>
        <location filename="../ui/widgetPropGeneralPlotSettings.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>General style settings </source>
        <translation type="unfinished">Allgemeine Style-Einstellungen </translation>
    </message>
    <message>
        <source>When opening any plot supported by itom a set of default style settings is loaded. Each plot decides wheter the setting is relevant or not for its type (e.g. the line style does not affect a 2D plot).</source>
        <translation type="vanished">Wenn über itom ein Plot geöffnet wird, werden diese Werte standardmäßig geladen. Dabei werden natürlich nur Werte berücksichtigt, die für den entsprechenden Typ des Plots relevant sind (so z. B. hat der Linienstil keinen Effekt auf einen 2D-Plot).</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>Tracker Settings</source>
        <translation type="unfinished">Cursor-Positionsmarker</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Tracker background</source>
        <translation type="unfinished">Markerhintergrund</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Tracker pen color</source>
        <translation type="unfinished">Stiftfarbe des Markers</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Tracker font</source>
        <translation type="unfinished">Schriftart des Markers</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Zoom rubber band pen</source>
        <translation type="unfinished">Zoom-Rahmenstift</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Label color</source>
        <translation type="unfinished">Labelfarbe</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Shape label background</source>
        <translation type="unfinished">Labelhintergrund</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Shape label font</source>
        <translation type="unfinished">Labelschriftart</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Shape pen</source>
        <translation type="unfinished">Malstift</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Line style</source>
        <translation type="unfinished">Linienstil</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Legend label width</source>
        <translation type="unfinished">Breite der Legende</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Center marker size</source>
        <translation type="unfinished">Größe des zentrierten Fadenkreuzes</translation>
    </message>
    <message>
        <location line="-257"/>
        <source>When opening any plot supported by itom a set of default style settings is loaded. Each plot decides whehter the setting is relevant or not for its type (e.g. the line style does not affect a 2D plot). Plots, that are integrated in an ui-file, only consider these settings above, that cannot be set in the QtDesigner properties.</source>
        <translation type="unfinished">Wenn über itom ein Plot geöffnet wird, werden diese Werte standardmäßig geladen. Dabei werden natürlich nur Werte berücksichtigt, die für den entsprechenden Typ des Plots relevant sind (so z. B. hat der Linienstil keinen Effekt auf einen 2D-Plot). Plots, die in eine UI-Datei integriert wurden, können nicht über die QtDesigner-Optionen gesetzt werden.</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Center marker pen</source>
        <translation type="unfinished">Stil des zentrierten Fadenkreuzes</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Print and Export</source>
        <translation type="unfinished">Druck und Export</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Clipboard resolution</source>
        <translation type="unfinished">Auflösung beim Kopieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This resolution is used, if the plot is copied to the clipboard</source>
        <translation type="unfinished">Diese Auflösung wird benutzt, wenn ein Plot in die Zwischenablage kopiert wird</translation>
    </message>
    <message>
        <location line="+3"/>
        <source> dpi</source>
        <translation></translation>
    </message>
    <message>
        <location line="-350"/>
        <source>Label font</source>
        <translation type="unfinished">Schriftart für Label</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Title font</source>
        <translation type="unfinished">Schriftart des Titels</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Axis font</source>
        <translation type="unfinished">Schriftart der Achsen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Legend font</source>
        <translation type="unfinished">Schriftart der Legende</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Geometric Shape Settings</source>
        <translation type="unfinished">Einstellungen der Geometrischen Formen</translation>
    </message>
    <message>
        <location line="+138"/>
        <source>1D Plot</source>
        <translation></translation>
    </message>
    <message>
        <location line="-69"/>
        <source>2D Plot</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Z stack marker pen</source>
        <translation type="unfinished">Stil der Z-Stapelmarkierung</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Z stack marker size</source>
        <translation type="unfinished">Größe der Z-Stapelmarkierung</translation>
    </message>
    <message>
        <location line="-225"/>
        <source>Axes Settings</source>
        <translation type="unfinished">Achsenbeschriftung</translation>
    </message>
    <message>
        <location line="+391"/>
        <source>Set To Default</source>
        <translation type="unfinished">Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralStyles</name>
    <message>
        <location filename="../ui/widgetPropGeneralStyles.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stylesheets</source>
        <translation>Formatvorlagen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No stylesheet</source>
        <translation>Keine Formatvorlage</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Pre-defined stylesheet:</source>
        <translation>Vordefinierte Formatvorlage:</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>User-defined stylesheet:</source>
        <translation>Benutzerdefinierte Formatvorlage:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stylesheet file (*.qss):</source>
        <translation>Formatvorlagendatei (*.qss):</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stylesheet file (*.qss)</source>
        <translation>Formatvorlagendatei (*.qss)</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+13"/>
        <source>Resource file (*.rcc)</source>
        <translation>Ressourcendatei (*.rcc)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Icon Theme</source>
        <translation>Hintergrundfarbe für Icons (Icon Theme)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Selected icon theme:</source>
        <translation>Icon Theme auswählen:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>icon theme for bright background [default]</source>
        <translation>Icon-Hintergrund für hellen Hintergrund [standard]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>icon theme for dark background</source>
        <translation>Icon-Hintergrund für dunklen Hintergrund</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Changes to styles and themes will only be applied after a restart of the application!</source>
        <translation>Änderungen des Erscheinungsbilds werden erst nach einem Neustart der Anwendung aktiv!</translation>
    </message>
</context>
<context>
    <name>WidgetPropHelpDock</name>
    <message>
        <location filename="../ui/widgetPropHelpDock.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Visible items in help viewer</source>
        <translation type="unfinished">In der Hilfe angezeigte Elemente</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>show plugin algorithms</source>
        <translation type="unfinished">Algorithmus-Plugins anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show plugin dataIO and actuators</source>
        <translation type="unfinished">DataIO- und Motor-Plugins anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show plugin widgets</source>
        <translation type="unfinished">Widget-Plugins anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show script references of python packages (see below)</source>
        <translation type="unfinished">Skript-Referenzen zu Python Paketen anzeigen (siehe unten)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>General options</source>
        <translation type="unfinished">Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Script references of python packages</source>
        <translation type="unfinished">Verfügbare Skript-Referenzen von Python Paketen</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Locate local script reference on disk</source>
        <translation type="unfinished">Speicherort der lokalen Skript-Referenz öffnen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Remove local script reference</source>
        <translation type="unfinished">Lokale Skript-Referenz entfernen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Download update</source>
        <translation type="unfinished">Update herunterladen</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Update list of available help databases</source>
        <translation type="unfinished">Liste verfügbarer Updates der Hilfedatenbank</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Timeout:</source>
        <translation type="unfinished">Zeitüberschreitung:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source> s</source>
        <translation type="unfinished"> s</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Refresh list</source>
        <translation type="unfinished">Liste aktualisieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Local: The .db-file is saved in the help directory of your itom installation. Check an entry to show it in the help viewer (if &apos;show script references for python packages&apos; is selected above).</source>
        <translation type="unfinished">Lokal: Die Datenbankdatei (.db) liegt im -Unterverzeichnis &apos;help&apos; der itom-Installation. Markierte Einträge werden in der Hilfe angezeigt, sofern der allgemeine Haken bei &apos;Skript-Referenzen zu Python Paketen anzeigen&apos; (oben) gesetzt  ist.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remote: Available database files for script references of various python packages. Right click an entry and select &apos;download&apos; to get the database and save it locally in the help directory of your itom installation.</source>
        <translation type="unfinished">Online: Verfügbare Datenbank-Dateien für Skript Referenzen verschiedener Python Pakete. Nach einem Rechts-Klick auf einen Eintrag, kann die Option &apos;Herunterladen&apos; gewählt werden, um die jeweilige Datenbank herunterzuladen und lokal im Unterordner &apos;help&apos; der itom Installation zu speichern.</translation>
    </message>
    <message>
        <source>List of all Modules in help-directory</source>
        <translation type="vanished">Liste aller Module im Hilfeverzeichnis</translation>
    </message>
    <message>
        <source>Behaviour-Options</source>
        <translation type="vanished">Verhaltensoptionen</translation>
    </message>
    <message>
        <location line="-250"/>
        <source>Open external links</source>
        <translation>Externe Links öffnen</translation>
    </message>
    <message>
        <source>Show Modules and Packages</source>
        <translation type="vanished">Module und Packages anzeigen</translation>
    </message>
    <message>
        <source>Show Algorithms</source>
        <translation type="vanished">Algorithmus anzeigen</translation>
    </message>
    <message>
        <source>Show Widgets</source>
        <translation type="vanished">Widget anzeigen</translation>
    </message>
    <message>
        <source>Show DataIO and Actuator</source>
        <translation type="vanished">DataIO und Motoren anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Display Plaintext (html-source)</source>
        <translation>Klartext anzeigen (HTML-Quelle)</translation>
    </message>
    <message>
        <source>Help-Update configuration</source>
        <translation type="vanished">Konfiguration des Hilfe-Updates</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Server:</source>
        <translation type="unfinished">Server:</translation>
    </message>
    <message>
        <source>Download Timeout in sec:</source>
        <translation type="vanished">Zeitüberschreitung für Download (Sek.):</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Aktualisieren</translation>
    </message>
</context>
<context>
    <name>WidgetPropPalettes</name>
    <message>
        <location filename="../ui/widgetPropPalettes.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Fenster</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Edit an existing palette or create a new one</source>
        <translation type="unfinished">Eine vorhandene Palette bearbeiten oder eine neue erstellen</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Add New Palette</source>
        <translation type="unfinished">Neue Palette hinzufügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Duplicate Selected</source>
        <translation type="unfinished">Duplikate auswählen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove Selected</source>
        <translation type="unfinished">Auswahl entfernen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Import Palette</source>
        <translation type="unfinished">Palette importieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Export Selected</source>
        <translation type="unfinished">Auswahl exportieren</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Palette options:</source>
        <translation type="unfinished">Palettenoption:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Color stops:</source>
        <translation type="unfinished">Knoten:</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Add</source>
        <translation type="unfinished">Hinzufügen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Remove</source>
        <translation type="unfinished">Entfernen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Equidistant</source>
        <translation type="unfinished">Abstandsgetreu</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Inverse Color 1</source>
        <translation type="unfinished">Inverse Farbe 1</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+31"/>
        <location line="+31"/>
        <source>##</source>
        <translation></translation>
    </message>
    <message>
        <location line="-55"/>
        <location line="+31"/>
        <location line="+31"/>
        <source>#000000</source>
        <translation></translation>
    </message>
    <message>
        <location line="-50"/>
        <source>Inverse Color 2</source>
        <translation type="unfinished">Inverse Farbe 2</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Apply Changes</source>
        <translation type="unfinished">Übernehmen</translation>
    </message>
    <message>
        <location line="-135"/>
        <source>Invalid Color (NaN)</source>
        <translation type="unfinished">Farbe Ungültig (NaN)</translation>
    </message>
</context>
<context>
    <name>WidgetPropPluginsAlgorithms</name>
    <message>
        <location filename="../ui/widgetPropPluginsAlgorithms.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Parallelization</source>
        <translation>Parallelisierung</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Maximum number of threads used for parallelization:</source>
        <translation>Maximale Anzahl Threads für die Parallelisierung:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of available threads (incl. virtual threads):</source>
        <translation>Anzahl verfügbarer Threads (inkl. Virtuellen):</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Some filters or algorithms can use multiple CPU cores to process in parallel.</source>
        <translation>Einige Filter und Algorithmen können mehrere Prozessorkerne parallel nutzen.</translation>
    </message>
</context>
<context>
    <name>WidgetPropPythonGeneral</name>
    <message>
        <location filename="../ui/widgetPropPythonGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Executing or debugging scripts with unsaved changes</source>
        <translation>Speicherverhalten beim Ausführen oder Debuggen von Skripts mit nicht gespeicherten Änderungen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Always ask to save scripts before execution</source>
        <translation>Vor dem Ausführen immer fragen ob das Skript gespeichert werden soll</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Automatically save all unsaved scripts before execution</source>
        <translation>Vor dem Ausführen das Skript immer speichern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Do not save any unsaved scripts (last saved version is executed then)</source>
        <translation>Änderungen im Skript nicht speichern (letzte gespeicherte Version wird ausgeführt!)</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Python home directory</source>
        <translation>Python-Verzeichnis</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Python can only be started properly if its home directory, containing built-in libraries, is automatically detected or manually given. Please choose the right option for finding this directory here (under Windows, the directory usually contains the Python executable):</source>
        <translation>Python kann nur gestartet werde, wenn das Python-Verzeichnis, welches die Built-In-Bibliotheken enthält, automatisch erkannt oder manuell angegeben wurde, Bitte die korrekte Option auswählen (in Windows enthält das benötigte Verzeichnis die Python.exe):</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Subdirectory of itom (...\itom\python3)</source>
        <translation>Unterverzeichnis von itom (...\itom\python3)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>System information (Registry, PYTHONHOME,...)</source>
        <translation>Systeminformationen (Registry, PYTHONHOME-Variable,...)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User defined:</source>
        <translation>Benutzerdefiniert:</translation>
    </message>
</context>
<context>
    <name>WidgetPropPythonStartup</name>
    <message>
        <location filename="../ui/widgetPropPythonStartup.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Add File</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Move Up</source>
        <translation>Nach Oben</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Move Down</source>
        <translation>Nach Unten</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Add new files relative to itom base path: %1</source>
        <translation>Neue Dateien relativ zum itom Hauptverzeichnis laden: %1</translation>
    </message>
    <message>
        <source>Base path: </source>
        <translation type="vanished">Stammverzeichnis: </translation>
    </message>
    <message>
        <source>To change the order in which the items (files) are loaded, use Drag and Drop</source>
        <translation type="vanished">Um die Reihenfolger der geladenen Einträge zu ändern, bitte &apos;Drag &amp; Drop&apos; benutzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropWorkspaceUnpack</name>
    <message>
        <location filename="../ui/widgetPropWorkspaceUnpack.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Import IDC and MAT files to workspace</source>
        <translation>Import von IDC- und MAT-Dateien in den Workspace</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unpack dictionary</source>
        <translation>In ein &apos;Dictionary&apos; entpacken</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>When importing an *.idc or *.mat file to the workspace, it is either possible to unpack all values within the file and load them as separate variables to the workspace or to load the content of the file as one single dictionary (name of the dictionary will be requested by an input dialog)</source>
        <translation>Beim Import von *.idc oder *.mat-Dateien in den Workspace ist es entweder möglich alle Werte innerhalb der Datei als jeweils seperate Variablen direkt in den Workspace zu entpacken oder in ein eigenes &apos;Dictionary&apos; zu laden (der Name des &apos;Dictionary&apos; wird beim Import abgefragt)</translation>
    </message>
</context>
<context>
    <name>ito::AIManagerWidget</name>
    <message>
        <location filename="../widgets/AIManagerWidget.cpp" line="+80"/>
        <source>Configuration Dialog</source>
        <translation>Konfigurationsdialog</translation>
    </message>
    <message>
        <location line="+295"/>
        <source>Show Plugin Toolbox</source>
        <translation>Plugin-Toolbox anzeigen</translation>
    </message>
    <message>
        <location line="-283"/>
        <source>Close Instance</source>
        <translation>Instanz schließen</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>New Instance...</source>
        <translation>Neue Instanz...</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Live Image...</source>
        <translation>Livebild...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Snap Dialog...</source>
        <translation>Aufnahmedialog...</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Info...</source>
        <translation></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Open Widget...</source>
        <translation>Fenster öffnen...</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Hide Plugin Toolbox</source>
        <translation>Plugin-Toolbox ausblenden</translation>
    </message>
    <message>
        <location line="-281"/>
        <source>Show/Hide Plugin Toolbox</source>
        <translation>Plugin-Toolbox ein-/ausblenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Close All</source>
        <translation>Alles schließen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Auto Grabbing</source>
        <translation>Auto-Grabbing</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Send To Python...</source>
        <translation>An Python senden...</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location line="+191"/>
        <source>The instance &apos;%1&apos; cannot be closed by GUI since it has been created by Python</source>
        <translation>Die Instanz &apos;%1&apos; kann nicht über die GUI geschlossen werden, da diese durch Python erstellt wurde</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+4"/>
        <source>Closing not possible</source>
        <translation>Schließen nicht möglich</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The instance &apos;%1&apos; can temporarily not be closed since it is still in use by another element.</source>
        <translation>Die Instanz &apos;%1&apos; kann im Moment nicht geschlossen werden, da diese noch von anderen Elementen benutzt wird.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The instance &apos;%1&apos; can finally not be closed since there are still references to this instance from other componentents, e.g. python variables.</source>
        <translation>Die Instanz &apos;%1&apos; kann nicht endgültig geschlossen werden, da auf diese noch andere Komponenten (z. B. Python-Variablen) referenzieren.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning while closing instance. Message: %1</source>
        <translation>Warnung beim Schließen der Instanz. Meldung: %1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error while closing instance. Message: %1</source>
        <translation>Fehler beim Schließen der Instanz. Meldung: %1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Warning while showing configuration dialog. Message: %1</source>
        <translation>Beim Anzeigen des Konfigurationsdialogs ist eine Warnung aufgetreten. Nachricht: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while showing configuration dialog</source>
        <translation>Beim Anzeigen des Konfigurationsdialogs ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error while showing configuration dialog. Message: %1</source>
        <translation>Fehler beim Anzeigen des Konfigurationsdialogs. Nachricht: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error while showing configuration dialog</source>
        <translation>Fehler beim Anzeigen des Konfigurationsdialogs</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+39"/>
        <source>Error while creating new instance</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Warning while creating new instance</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+97"/>
        <source>Timeout</source>
        <translation>Zeitüberschreitung</translation>
    </message>
    <message>
        <location line="-97"/>
        <location line="+97"/>
        <source>Python did not response to the request within a certain timeout.</source>
        <translation>Python reagierte nicht innerhalb der zulässigen Zeit auf die Anfrag.</translation>
    </message>
    <message>
        <location line="-89"/>
        <location line="+97"/>
        <source>Warning while sending instance to python</source>
        <translation>Warnung beim Senden der Instanz an Python</translation>
    </message>
    <message>
        <location line="-92"/>
        <location line="+97"/>
        <source>Error while sending instance to python</source>
        <translation>Fehler beim Senden der Instanz an Python</translation>
    </message>
    <message>
        <location line="-92"/>
        <location line="+98"/>
        <source>Python not available</source>
        <translation>Python nicht verfügbar</translation>
    </message>
    <message>
        <location line="-98"/>
        <location line="+98"/>
        <source>The Python engine is not available</source>
        <translation>Die Python-Engine ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Could not find instance of UiOrganizer</source>
        <translation>Die Instanz des UI-Organizers konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>This instance is no grabber. Therefore no snap dialog is available.</source>
        <translation>Diese Instanz ist kein Grabber. Daher ist kein Aufnahmedialog verfügbar.</translation>
    </message>
    <message>
        <location line="-519"/>
        <source>final closing not possible</source>
        <translation>Endgültiges Schließen nicht möglich</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Warning while closing instance</source>
        <translation>Warnung beim Schließen der Instanz</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error while closing instance</source>
        <translation>Fehler beim Schließen der Instanz</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Error while creating new instance. 
Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz.
Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Warning while creating new instance. Message: %1</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error while creating new instance. Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+97"/>
        <source>Warning while sending instance to python. Message: %1</source>
        <translation>Warnung beim Senden der Instanz an Python. Meldung: %1</translation>
    </message>
    <message>
        <location line="-92"/>
        <location line="+97"/>
        <source>Error while sending instance to python. Message: %1</source>
        <translation>Fehler beim Senden der Instanz an Python. Meldung: %1</translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Python variable name</source>
        <translation>Variablenname in Python</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python variable name for saving this instance in global workspace</source>
        <translation>Variablenname in Python um diese Instanz in den globalen Workspace zu schreiben</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>instance</source>
        <translation>Instanz</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>User interface of plugin could not be created. Returned handle is invalid.</source>
        <translation>Benutzerschnittstelle des Plugins konnte nicht erstellt werden. Gesendetes Handle ist ungültig.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error while opening user interface from plugin.</source>
        <translation>Fehler beim Öffnen der Benutzerschnittstelle des Plugins.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning while opening user interface from plugin.</source>
        <translation>Warnung beim Öffnen der Benutzerschnittstelle des Plugins.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>This instance is no grabber. Therefore no live image is available.</source>
        <translation>Diese Instanz ist kein Grabber. Daher ist kein Livebild verfügbar.</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDockWidget</name>
    <message>
        <location filename="../widgets/abstractDockWidget.cpp" line="+116"/>
        <source>Stay On Top</source>
        <translation>Im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stay on top of all visible windows</source>
        <translation>Alle sichtbaren Fenster im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Stay On Top Of Main Window</source>
        <translation>Hauptfenster im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stay on top of main window of itom</source>
        <translation>Hauptfenster von itom im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Docking Toolbar</source>
        <translation>Symbolleiste Fenster andocken</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Dock Widget</source>
        <translation>Fenster eindocken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undock Widget</source>
        <translation>Fenster ausdocken</translation>
    </message>
    <message>
        <location line="+490"/>
        <source>Toolbar &apos;%1&apos; is already available</source>
        <translation>Die Symbolleiste &apos;%1&apos; ist bereits vorhanden</translation>
    </message>
    <message>
        <location line="+124"/>
        <source>Toolbar &apos;%1&apos; not found</source>
        <translation>Die Symbolleiste &apos;%1&apos; wurde nicht gefunden</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFilterDialog</name>
    <message>
        <location filename="../ui/abstractFilterDialog.cpp" line="+109"/>
        <location line="+10"/>
        <location line="+10"/>
        <location line="+41"/>
        <location line="+20"/>
        <source>size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>dims</source>
        <translation>Dimensionen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>type</source>
        <translation>Typ</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="vanished">Nicht-Inizialisierte Vektoren bei Pflichtparametern!</translation>
    </message>
    <message>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="vanished">Nicht inizialisierte Vektoren bei optionalen Parametern!</translation>
    </message>
    <message>
        <source>uninitialized vector for output parameters!</source>
        <translation type="vanished">Nicht inizialisierte Vektoren bei Rückgabeparametern!</translation>
    </message>
</context>
<context>
    <name>ito::BreakPointDockWidget</name>
    <message>
        <location filename="../widgets/breakPointDockWidget.cpp" line="+146"/>
        <source>Breakpoints</source>
        <translation>Haltepunkte</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Delete Breakpoint</source>
        <translation>Haltepunkt löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Delete All Breakpoints</source>
        <translation>Alle Haltepunkte löschen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>En- Or Disable Breakpoint</source>
        <translation>Haltepunkt ein- und ausschalten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>En- Or Disable All Breakpoints</source>
        <translation>Alle Haltepunkte ein- und ausschalten</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Edit Breakpoints</source>
        <translation>Haltepunkte bearbeiten</translation>
    </message>
</context>
<context>
    <name>ito::BreakPointModel</name>
    <message>
        <location filename="../models/breakPointModel.cpp" line="+72"/>
        <source>Line</source>
        <translation>Zeile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Condition</source>
        <translation>Anforderungen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Temporary</source>
        <translation>Temporär</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Enabled</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ignore count</source>
        <translation>Anzahl Ignorierungen</translation>
    </message>
    <message>
        <location line="+326"/>
        <location line="+2"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+2"/>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <location line="+449"/>
        <source>Given modelIndex of breakpoint is invalid</source>
        <translation>Der übergebene &apos;modelIndex&apos; ist kein Index eines Haltepunkts</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Filename must not be changed</source>
        <translation>Der Dateiname darf nicht geändert werden</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Given modelIndex is no model index of a breakpoint</source>
        <translation>Der übergebene &apos;modelIndex&apos; ist kein Index eines Haltepunkts</translation>
    </message>
</context>
<context>
    <name>ito::BreakpointPanel</name>
    <message>
        <location filename="../codeEditor/panels/breakpointPanel.cpp" line="+76"/>
        <source>&amp;Toggle Breakpoint</source>
        <translation type="unfinished">Haltepunkt ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+138"/>
        <source>&amp;Disable Breakpoint</source>
        <translation type="unfinished">Haltepunkt &amp;deaktivieren</translation>
    </message>
    <message>
        <location line="-137"/>
        <source>&amp;Edit Condition</source>
        <translation type="unfinished">Bedingungen &amp;bearbeiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next Breakpoint</source>
        <translation type="unfinished">&amp;Nächster Haltepunkt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Previous Breakpoint</source>
        <translation type="unfinished">&amp;Vorheriger Haltepunkt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete All Breakpoints</source>
        <translation type="unfinished">Alle Haltepunkte &amp;löschen</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>&amp;Enable Breakpoint</source>
        <translation type="unfinished">Haltepunkt &amp;aktivieren</translation>
    </message>
</context>
<context>
    <name>ito::CallStackDockWidget</name>
    <message>
        <location filename="../widgets/callStackDockWidget.cpp" line="+62"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Line</source>
        <translation>Zeile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Method</source>
        <translation>Methode</translation>
    </message>
</context>
<context>
    <name>ito::CheckerBookmarkPanel</name>
    <message>
        <location filename="../codeEditor/panels/checkerBookmarkPanel.cpp" line="+70"/>
        <source>&amp;Toggle Bookmark</source>
        <translation type="unfinished">Lesezeichen ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Bookmark</source>
        <translation type="unfinished">Nächstes Lesezeichen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Bookmark</source>
        <translation type="unfinished">Vorheriges Lesezeichen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear All Bookmarks</source>
        <translation type="unfinished">Alle Lesezeichen löschen</translation>
    </message>
</context>
<context>
    <name>ito::CodeCompletionMode</name>
    <message>
        <location filename="../codeEditor/modes/codeCompletion.cpp" line="+884"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>ito::ColCurve</name>
    <message>
        <location filename="../ui/widgetPropPalettes.cpp" line="+192"/>
        <source>Context menu</source>
        <translation>Kontextmenü</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete Color Stop</source>
        <translation>Knoten entfernen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add color stop</source>
        <translation>Knoten hinzufügen</translation>
    </message>
</context>
<context>
    <name>ito::ConsoleWidget</name>
    <message>
        <location filename="../widgets/consoleWidget.cpp" line="+1078"/>
        <location line="+16"/>
        <source>Python is not available</source>
        <translation>Python ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-16"/>
        <location line="+16"/>
        <source>Script Execution</source>
        <translation>Skript wird ausgeführt</translation>
    </message>
    <message>
        <location line="+692"/>
        <source>&amp;Undo</source>
        <translation>&amp;Rückgängig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>&amp;Wiederherstellen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Cut</source>
        <translation>&amp;Ausschneiden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cop&amp;y</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Command Line</source>
        <translation>Konsole leeren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select All</source>
        <translation type="unfinished">Alles auswählen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Auto Scroll</source>
        <translation>Auto-Scroll</translation>
    </message>
</context>
<context>
    <name>ito::DesignerWidgetOrganizer</name>
    <message>
        <location filename="../organizer/designerWidgetOrganizer.cpp" line="-50"/>
        <source>could not read interface &apos;ito.AbstractItomDesignerPlugin&apos;</source>
        <translation>Die Schnittstelle &apos;ito.AbstractItomDesignerPlugin&apos; konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>DesignerWidget &apos;%1&apos; successfully loaded</source>
        <translation>Designer-Widget &apos;%1&apos; erfolgreich geladen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The version &apos;ito.AbstractItomDesignerPlugin&apos; in file &apos;%1&apos; does not correspond to the requested version (%2)</source>
        <translation>Die Version von &apos;ito.AbstractItomDesignerPlugin&apos; in der Datei &apos;%1&apos; deckt sich nicht mit der erforderlichen Version (%2)</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Plugin in file &apos;%1&apos; is no Qt DesignerWidget inherited from QDesignerCustomWidgetInterface</source>
        <translation>Das Plugin in der Datei &apos;%1&apos; ist kein von Qt DesignerWidget abgeleitetes QDesignerCustomWidgetInterface</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Plugin in file &apos;%1&apos; is a Qt Designer widget but no itom plot widget that inherits &apos;ito.AbstractItomDesignerPlugin&apos;</source>
        <translation>Das Plugin in der Datei &apos;%1&apos; ist ein Qt DesignerWidget, aber kein von &apos;ito.AbstractItomDesignerPlugin&apos; abgeleitetes itom-Plot-Widget</translation>
    </message>
    <message>
        <location line="+61"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Figure &apos;%s&apos; does not correspond to the minimum requirements</source>
        <translation>Figure &apos;%s&apos; entspricht nicht den Mindestanforderungen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Figure &apos;%s&apos; not found</source>
        <translation>Figure &apos;%s&apos; nicht gefunden</translation>
    </message>
    <message>
        <location line="+76"/>
        <location line="+125"/>
        <source>The figure category &apos;%s&apos; is unknown</source>
        <translation>Die Figure-Kategorie &apos;%s&apos; ist unbekannt</translation>
    </message>
    <message>
        <location line="-67"/>
        <source>The figure class &apos;%1&apos; could not be found or does not support displaying the given type of data. The default class for the given data is used instead.</source>
        <translation>Die Klasse &apos;%1&apos; wurde nicht gefunden oder die Daten können nicht angezeigt werden. Es wird zur Anzeige der Daten die Standardklasse verwendet.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>no plot figure plugin could be found that fits to the given category.</source>
        <translation>Kein Plot-Figure-Plugin gefunden, welches mit der vorgegebenen Kategorie übereinstimmt.</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>DataObject - Line</source>
        <translation>DataObject - Linie</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>DataObject - Plane</source>
        <translation>DataObject - Fläche</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>DataObject - Plane Stack</source>
        <translation>DataObject - Fläschenstapel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Point Cloud</source>
        <translation>Punktewolke</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>PolygonMesh</source>
        <translation>Polygonnetz</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+54"/>
        <location line="+50"/>
        <location line="+34"/>
        <source>invalid type or no type defined</source>
        <translation>Ungültiger Typ oder keine Typendefinition</translation>
    </message>
    <message>
        <location line="-120"/>
        <source>Gray8</source>
        <translation>Grau8</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Gray16</source>
        <translation>Grau16</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Gray32</source>
        <translation>Grau32</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>RGB32</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ARGB32</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>CMYK32</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Float32</source>
        <translation>Fließkomma32</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Float64</source>
        <translation>Fließkomma64</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Complex</source>
        <translation>Komplex</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Static</source>
        <translation>Statisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Live</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cartesian</source>
        <translation>Katesisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Polar</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cylindrical</source>
        <translation>Zylindrisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>OpenGl</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cuda</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>X3D</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Line Plot</source>
        <translation>Linien-Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Image Plot</source>
        <translation>Bild-Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Isometric Plot</source>
        <translation>Isometrischer Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>3D Plot</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ito::DialogAboutQItom</name>
    <message>
        <location filename="../ui/dialogAbout.cpp" line="+45"/>
        <location line="+61"/>
        <location line="+15"/>
        <location line="+14"/>
        <source>Could not load file %1. Reason: %2.</source>
        <translation>Die Datei &apos;%1&apos; kann nicht geladen werden. Grund: %2.</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The version string has been copied to the clipboard</source>
        <translation>Die Versionsinformationen wurde in die Zwischenablage kopiert</translation>
    </message>
</context>
<context>
    <name>ito::DialogCloseItom</name>
    <message>
        <location filename="../ui/dialogCloseItom.cpp" line="+68"/>
        <source>Try to interrupt Python...</source>
        <translation>Versuch Python zu unterbrechen...</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Python did not stop. Do you want to retry to interrupt Python?</source>
        <translation>Python wurde nicht gestoppt. Soll erneut versucht werden Python zu unterbrechen?</translation>
    </message>
</context>
<context>
    <name>ito::DialogGoto</name>
    <message>
        <location filename="../ui/dialogGoto.cpp" line="+36"/>
        <source>Line number (1 - %1, current: %2):</source>
        <translation>Zeilennummer (1 - %1, aktuell: %2):</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Character number (0 - %1, current: %2):</source>
        <translation>Zeichennummer (1 - %1, aktuell: %2):</translation>
    </message>
</context>
<context>
    <name>ito::DialogIconBrowser</name>
    <message>
        <location filename="../ui/dialogIconBrowser.cpp" line="+78"/>
        <source>Icon Browser</source>
        <translation>Icon-Suche</translation>
    </message>
</context>
<context>
    <name>ito::DialogOpenFileWithFilter</name>
    <message>
        <location filename="../ui/dialogOpenFileWithFilter.cpp" line="+62"/>
        <source>The name must start with a letter followed by numbers or letters [a-z] or [A-Z]</source>
        <translation>Der Name muss mit einem Buchstaben, gefolgt von Ziffern oder Buchstaben [a-z] oder [A-Z], beginnen</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Python variable name missing</source>
        <translation>Python Variablenname fehlt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You have to give a variable name, under which the loaded item is saved in the global workspace</source>
        <translation>Für das globalen Workspace muss ein Variablenname vergeben werden</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+8"/>
        <source>Python variable name already exists</source>
        <translation>Der Variablenname existiert bereits bei Python</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>The variable name %1 already exists in this workspace. Do you want to overwrite it?</source>
        <translation>Die Variable &apos;%1&apos; existiert bereits in diesem Workspace. Soll diese überschrieben werden?</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The variable name %1 already exists in this workspace. It cannot be overwritten since it is a function, method, type or class. Choose a new name.</source>
        <translation>Die Variable &apos;%1&apos; existiert bereits in diesem Workspace. Diese kann nicht überschrieben werden, da sie eine Funktion, Methode, Typ oder Klasse repräsentiert. Bitte einen anderen Namen wählen.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Timeout while verifiying variable name</source>
        <translation>Zeitüberschreitung während der Prüfung der Variablennamen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>A timeout occurred while checking for the existence of the variable name in Python. Please try it again.</source>
        <translation>Zeitüberschreitung während der Prüfung auf existierende Variablennamen in Python. Bitte nochmals versuchen.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>loading...</source>
        <translation>laden...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid parameters.</source>
        <translation>Ungültige Parameter.</translation>
    </message>
    <message>
        <location line="+36"/>
        <location line="+25"/>
        <source>An error occurred while loading the file.</source>
        <translation>Beim Laden der Datei ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="-23"/>
        <location line="+25"/>
        <source>Error while loading file</source>
        <translation>Fehler beim Laden der Datei</translation>
    </message>
    <message>
        <location line="-21"/>
        <location line="+34"/>
        <source>A warning occurred while loading the file.</source>
        <translation>Beim Laden der Datei ist eine Warnung aufgetreten.</translation>
    </message>
    <message>
        <location line="-32"/>
        <location line="+34"/>
        <source>Warning while loading file</source>
        <translation>Warnung beim Dateiladen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Procedure still running</source>
        <translation>Wird noch ausgeführt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file is still being loaded. Please wait...</source>
        <translation>Die Datei wird noch geladen. Bitte warten...</translation>
    </message>
</context>
<context>
    <name>ito::DialogPipManager</name>
    <message>
        <location filename="../ui/dialogPipManager.cpp" line="+111"/>
        <source>Python initialization error</source>
        <translation>Inizialisierungsfehler von Python</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>A proxy server was restored from the settings file. This proxy contained a password, which was not saved in the settings. Please set it again in the pip settings (below).</source>
        <translation type="unfinished">In der Ini-Datei des aktuellen itom-Benutzers wurde ein Proxy-Server ohne Passwort angelegt. Bitte unter den Pip-Einstellungen (siehe unten) den Proxy erneut angeben.</translation>
    </message>
    <message>
        <location line="+217"/>
        <source>Abort</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The pip process is still running. Do you want to interrupt it?</source>
        <translation>Der Pip-Prozess läuft bereits. Soll dieser abgebrochen werden?</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Pip Manager</source>
        <translation>Pip-Manager</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning installing Numpy if itom is already running.</source>
        <translation>Warnung bei der Installation von Numpy bei laufendem itom.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>If you try to install / upgrade Numpy if itom is already running, a file access error might occur, since itom already uses parts of Numpy. 

You have now three possibilities: 
1. Try to continue the installation in spite of possible problems by clicking &apos;Ignore&apos; 
2. Click &apos;OK&apos;, close and restart itom. Then this package manager is opened as standalone application and you can install or upgrade Numpy and other packages. After another restart, itom is restarted as usual. 
3. Click &apos;Cancel&apos; to cancel the installation process without any changes. 

Information: 
If the case of the restart (&apos;OK&apos;), an empty file &apos;restart_itom_with_pip_manager.txt&apos; is created in the directory &apos;%1&apos;. If itom locates this file at startup, the pip manager is directly started. 

It is also possible to directly start the package manager by calling the itom application with the argument &apos;pipManager&apos;.</source>
        <translation>Die Installation oder das Updaten von Numpy im laufenden itom könnte einen Fehler verursachen, wenn Teile von Numby bereits verwendet werden. 

Es gibt nun drei Möglichkeiten:
1.Durch Klick auf &apos;Ignorieren&apos; versuchen die Installation fortzusetzen.
2. &apos;OK&apos; anklicken, itom schließen und neustarten. Daraufhin öffnet sich der Paket-Manager im &apos;Standalone&apos;-Modus und die Installation oder das Upgrade von Numpy und anderen Paketen sind möglich. Nach einem weitern Neustart wird sich wie gewohnt itom öffnen.
3. Den Installationsprozess ohne Ändernungen abbrechen.

Hinweis:
Nach dem Klick auf &apos;OK&apos; wird die Datei &apos;restart_itom_with_pip_manager.txt&apos; im Verzeichnis &apos;%1&apos; erstellt. Wird diese beim nächsten itom-Start entdeckt, startet der Pip-Manager direkt.

Es ist ebenfalls möglich über das Argument &apos;pipManager&apos; beim Programmstart den Paket-Manager direkt zu starten.</translation>
    </message>
    <message>
        <location line="+54"/>
        <location line="+7"/>
        <location line="+24"/>
        <location line="+7"/>
        <source>Uninstall package</source>
        <translation>Paket deinstallation</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+31"/>
        <source>The package &apos;%1&apos; is used by at least one other package. Do you really want to uninstall it?</source>
        <translation>Das Paket &apos;%1&apos; wird von mindestens einem anderen Paket gebraucht. Soll es wirklich deinstalliert werden?</translation>
    </message>
    <message>
        <location line="-24"/>
        <location line="+31"/>
        <source>Do you really want to uninstall the package &apos;%1&apos;?</source>
        <translation>Soll das Paket &apos;%1&apos; wirklich deinstalliert werden?</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Export table to clipboard</source>
        <translation type="unfinished">Tabelle in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Export table to csv-file...</source>
        <translation type="unfinished">Tabelle als CSV-Datei exportieren...</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>CSV files (*.csv);;All files (*.*)</source>
        <translation type="unfinished">CVS-Datei (*.cvs);;Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CSV files (*.csv)</source>
        <translation type="unfinished">CVS-Datei (*.cvs)</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+17"/>
        <source>Export to file</source>
        <translation type="unfinished">In Datei exportieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file &apos;%s&apos; could not be opened</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; konnte nicht geöffnet werden</translation>
    </message>
</context>
<context>
    <name>ito::DialogPipManagerInstall</name>
    <message>
        <location filename="../ui/dialogPipManagerInstall.cpp" line="+48"/>
        <source>Install Package</source>
        <translation>Paket installieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Package</source>
        <translation>Paket updaten</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Select package archive</source>
        <translation>Paket-Archiv auswählen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>choose whl archive...</source>
        <translation>Whl-Archiv wählen...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>choose tar.gz or zip archive...</source>
        <translation>Tar.gz-Archiv wählen...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>package-name</source>
        <translation>Paketname</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Missing package</source>
        <translation>Fehlendes Paket</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You need to indicate a package</source>
        <translation>Es muss ein Paket ausgewählt sein</translation>
    </message>
</context>
<context>
    <name>ito::DialogPluginPicker</name>
    <message>
        <location filename="../ui/dialogPluginPicker.cpp" line="+178"/>
        <source>Error while creating new instance. 
Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz.
Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+39"/>
        <source>Error while creating new instance</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Warning while creating new instance. Message: %1</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error while creating new instance. Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Choose plugin</source>
        <translation>Plugin auswählen</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Warning while creating new instance</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Please choose plugin you want to create a new instance from</source>
        <translation>Bitte Plugin zur Erzeugung einer neuen Instanz auswählen</translation>
    </message>
</context>
<context>
    <name>ito::DialogProperties</name>
    <message>
        <location filename="../ui/dialogProperties.cpp" line="+59"/>
        <source>Properties</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+7"/>
        <location line="+4"/>
        <location line="+2"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Editor - General</source>
        <translation>Editor - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Script Editors</source>
        <translation>Skript-Editor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Calltips</source>
        <translation>Vorschläge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Auto Completion</source>
        <translation>Autovervollständigung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Styles</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Console</source>
        <translation>Konsole</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Editor - Please Choose Subpage</source>
        <translation>Editor - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Editor - Scripts</source>
        <translation>Editor - Skripte</translation>
    </message>
    <message>
        <source>Editor - API Files</source>
        <translation type="vanished">Editor - API-Dateien</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor - Calltips</source>
        <translation>Editor - Vorschläge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor - Auto Completion</source>
        <translation>Editor - Autovervollständigung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Goto Assignment</source>
        <translation type="unfinished">Gehe zu Definition/Zuweisung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - Goto Assignment</source>
        <translation type="unfinished">Editor - Gehe zu Definition/Zuweisung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editor - Styles</source>
        <translation>Editor - Ansicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Console - Please Choose Subpage</source>
        <translation>Konsole - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Console - General</source>
        <translation>Konsole - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Line Wrap</source>
        <translation>Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Console - Line Wrap</source>
        <translation>Konsole - Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command History</source>
        <translation>Befehlsliste</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Console - Command History</source>
        <translation>Konsole - Befehlsliste</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python - Please Choose Subpage</source>
        <translation>Python - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Python - General</source>
        <translation>Python - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Python - Startups</source>
        <translation>Python - Autostart</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>General - Please Choose Subpage</source>
        <translation>Allgemein - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>General - Application</source>
        <translation>Allgemein - Anwendung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>General - Language</source>
        <translation>Allgemein - Sprache</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>General - Styles And Themes</source>
        <translation>Allgemein - Themen und Stile</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Workspace - Please Choose Subpage</source>
        <translation>Workspace - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugins - Please Choose Subpage</source>
        <translation>Plugins - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Algorithms And Filters</source>
        <translation>Algorithmen und Filter</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plugins - Algorithms And Filters</source>
        <translation>Plugins - Algorithmen und Filter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plots And Figures</source>
        <translation>Plots und Figures</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plots And Figures - Please Choose Subpage</source>
        <translation>Plots und Figures - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plots And Figures - Defaults</source>
        <translation>Plots und Figures - Standard</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Default Style Settings</source>
        <translation>Standard Style-Einstellungen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plots And Figures - Default Style Settings</source>
        <translation>Plots und Figures - Standard Style-Einstellungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Palettes Settings</source>
        <translation type="unfinished">Paletteneinstellungen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plots And Figures - Palettes Settings</source>
        <translation type="unfinished">Plots und Grafiken - Paletteneinstellungen</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Startup</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Help Viewer</source>
        <translation>Hilfeanzeige</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>General - Help Viewer</source>
        <translation>Allgemein - Hilfeanzeige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Styles and Themes</source>
        <translation>Themen und Stile</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Workspace</source>
        <translation>Workspace</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Import to workspace</source>
        <translation>Import in den Workspace</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Workspace - Import</source>
        <translation>Workspace - Import</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugins</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Default Plots</source>
        <translation>Standard-Plots</translation>
    </message>
</context>
<context>
    <name>ito::DialogReloadModule</name>
    <message>
        <location filename="../ui/dialogReloadModule.cpp" line="+68"/>
        <location line="+75"/>
        <source>Python Engine is invalid</source>
        <translation>Python-Engine ist ungültig</translation>
    </message>
    <message>
        <location line="-75"/>
        <location line="+75"/>
        <source>The Python Engine could not be found</source>
        <translation>Python-Engine wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>Connection problem</source>
        <translation>Verbindungsproblem</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>Error while getting module list</source>
        <translation>Fehler beim Lesen der Modulliste</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>connection problem</source>
        <translation>Verbindungsproblem</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+4"/>
        <source>Error while reloading modules</source>
        <translation>Fehler beim erneuten Laden der Module</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&lt;click on item to view detailed information&gt;</source>
        <translation type="unfinished">&lt;für detailiertere Informationen auf das Item klicken&gt;</translation>
    </message>
    <message>
        <location line="-111"/>
        <source>No information about loaded modules could be retrieved by python.</source>
        <translation>Keine Informationen über geladene Module von Python verfügbar.</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Module Name</source>
        <translation type="unfinished">Modulname</translation>
    </message>
    <message>
        <location line="+49"/>
        <location line="+74"/>
        <source>Unknown error</source>
        <translation>Unbekannter Fehler</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Timeout while forcing python to reload modules.</source>
        <translation>Zeitüberschreitung beim erneuten Laden der Module.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Module reload</source>
        <translation>Module erneut laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The following modules could not be reloaded:
</source>
        <translation>Folgende Module konnten nicht geladen werden:</translation>
    </message>
</context>
<context>
    <name>ito::DialogReplace</name>
    <message>
        <location filename="../ui/dialogReplace.cpp" line="+295"/>
        <source>Expand</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Collapse</source>
        <translation>Einfach</translation>
    </message>
    <message>
        <source>Collaps</source>
        <translation type="vanished">Einfach</translation>
    </message>
</context>
<context>
    <name>ito::DialogSelectUser</name>
    <message>
        <location filename="../ui/dialogSelectUser.cpp" line="+106"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+69"/>
        <location line="+13"/>
        <source>Wrong password</source>
        <translation type="unfinished">Falsches Passwort</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+13"/>
        <source>Wrong password, select another user or try again</source>
        <translation type="unfinished">Falsche Passwort. Bitte einen anderen Benutzer wählen oder das Passwort nochmals eingeben</translation>
    </message>
    <message>
        <source>Wrong password, select user or try again</source>
        <translation type="obsolete">Falsche Passwort. Bitte einen Benutzer wählen oder das Passwort nochmals eingeben</translation>
    </message>
</context>
<context>
    <name>ito::DialogSnapshot</name>
    <message>
        <location filename="../ui/dialogSnapshot.cpp" line="+71"/>
        <source>designerWidgetOrganizer is not available</source>
        <translation>&apos;designerWidgetOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Workspace</source>
        <translation></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The name must start with a letter followed by numbers or letters [a-z] or [A-Z]</source>
        <translation>Der Name muss mit einem Buchstaben, gefolgt von Ziffern oder Buchstaben [a-z] oder [A-Z], beginnen</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>acquire image %1 from %2</source>
        <translation>Aufnahme %1 von %2</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>The acquired image must be two-dimensional for a stack-acquisition</source>
        <translation>Für einen Bilderstapel muss das aufgenommene Bild zweidimensional sein</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Acquisition could not be finished. Wrong allocated stack size.</source>
        <translation>Die Aufnahme konnte nicht beendet werden. Falsch zugeteilte Stapelgröße.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Please stop the acquisition before closing the dialog</source>
        <translation>Die Aufnahme muss beendet sein bevor der Dialog geschlossen werden kann</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>save image</source>
        <translation>Bilddatei speichern</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>UnixTimestamp%1</source>
        <translation type="unfinished">UnixZeitstempel%1</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+136"/>
        <source>Python was not found</source>
        <translation>Python wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Timeout while seaching file name at workspace</source>
        <translation>Zeitüberschreitung beim Suchen von Dateinamen im Workspace</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Timeout while writing picture to workspace</source>
        <translation>Zeitüberschreitung beim Schreiben in den Workspace</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>save image %1 from %2</source>
        <translation>Speichern %1 von %2</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Timeout while saving picture</source>
        <translation>Zeitüberschreitung beim Speichern</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Snapshot</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Select a directory...</source>
        <translation>Ein Verzeichnis auswählen...</translation>
    </message>
</context>
<context>
    <name>ito::DialogUserManagement</name>
    <message>
        <location filename="../widgets/userManagement.cpp" line="+171"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-150"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>User Management - Current User: </source>
        <translation>Benutzerverwaltung - Aktueller Benutzer: </translation>
    </message>
    <message>
        <location line="+41"/>
        <source>User ID not found, aborting!</source>
        <translation>Die Benutzer-ID wurde nicht gefunden! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User ID and ini file name mismatch, aborting!</source>
        <translation>Die Ini-Datei wurde nicht gefunden! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User name and ini file user name mismatch, aborting!</source>
        <translation>Der Benutzername und der Name in der Ini-Datei stimmen nicht überein! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot delete current user, aborting!</source>
        <translation>Der aktuelle Benutzer kann nicht gelöscht werden! Der Vorgang wurde abgebrochen!</translation>
    </message>
</context>
<context>
    <name>ito::DialogUserManagementEdit</name>
    <message>
        <location filename="../widgets/userManagementEdit.cpp" line="+58"/>
        <location line="+7"/>
        <location line="+6"/>
        <location line="+66"/>
        <location line="+23"/>
        <location line="+6"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Name is empty! Cannot create user!</source>
        <translation>Name ist leer! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <source>UserID already exists! Cannot create user!</source>
        <translation type="vanished">Die Benutzer-ID wurde bereits vergeben! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User ID already exists! Cannot create user!</source>
        <translation type="unfinished">Die Benutzer-ID existiert bereits! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No user name entered, aborting!</source>
        <translation>Kein Benutzername! Vorgang wird abgebrochen!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Error retrieving user settings file name. Startup scripts not written to file.</source>
        <translation type="unfinished">Fehler beim Schreiben. Der Dateiname für die Benutzerdatei (*.ini) ist falsch.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>UserOrganizer not found!</source>
        <translation>&quot;UserOrganizer&quot; wurde nicht gefunden!</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>User Management - New User</source>
        <translation>Benutzerverwaltung - Neuer Benutzer erstellen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>User Management - Edit User</source>
        <translation>Benutzerverwaltung - Benutzer bearbeiten</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Load python script</source>
        <translation type="unfinished">Pyhton-Skript laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python script (*.py)</source>
        <translation type="unfinished">Python-Skript (*.py)</translation>
    </message>
</context>
<context>
    <name>ito::FigureWidget</name>
    <message>
        <location filename="../widgets/figureWidget.cpp" line="+129"/>
        <source>subplot %1 (empty)</source>
        <translation>Subplot &apos;%1&apos; (leer)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Subplots</source>
        <translation>&amp;Subplots</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location line="+216"/>
        <location line="+171"/>
        <source>designer widget of class &apos;%s&apos; cannot plot objects of type dataObject</source>
        <translation>Designer-Widget der Klasse &apos;%s&apos; kann keine Objekte vom Typ DataObject anzeigen</translation>
    </message>
    <message>
        <location line="-314"/>
        <location line="+49"/>
        <location line="+110"/>
        <location line="+19"/>
        <location line="+183"/>
        <source>designerWidgetOrganizer is not available</source>
        <translation>&apos;designerWidgetOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-377"/>
        <source>designer widget of class &apos;%s&apos; cannot plot objects of type pointCloud</source>
        <translation>DesignerWidget der Klasse &apos;%s&apos; kann kein Objekt vom Typ PointCloud anzeigen</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>designer widget of class &apos;%s&apos; cannot plot objects of type polygonMesh</source>
        <translation>DesignerWidget der Klasse &apos;%s&apos; kann kein Objekt vom Typ  Polygonnetz anzeigen</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>camera is not available any more</source>
        <translation>Die Kamera ist nicht länger verfügbar</translation>
    </message>
    <message>
        <location line="+199"/>
        <source>areaRow out of range [0,%i]</source>
        <translation>&apos;areaRow&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>arealCol out of range [0,%i]</source>
        <translation>&apos;areaCol&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>subplot %1</source>
        <translation>Subplot %1</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>could not create designer widget of class &apos;%s&apos;</source>
        <translation>Es kann kein DesignerWidget der Klasse &apos;%s&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>designerWidgetOrganizer or uiOrganizer is not available</source>
        <translation>&apos;designerWidgetOrganizer&apos; oder &apos;uiOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>addInBase pointer is NULL</source>
        <translation>Der Pointer auf &apos;addInBase&apos; ist NULL</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>timeout while getting parameter &apos;%s&apos; from plugin</source>
        <translation>Zeitüberschreitung beim Lesen des Parameters &apos;%s&apos; vom Plugin</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>parameter &apos;%s&apos; is not defined in plugin</source>
        <translation>Parameter &apos;%s&apos; wurde im Plugin nicht definiert</translation>
    </message>
</context>
<context>
    <name>ito::FileDownloader</name>
    <message>
        <location filename="../helper/fileDownloader.cpp" line="+128"/>
        <location line="+116"/>
        <source>no network reply instance available</source>
        <translation>Fehler beim Herunterladen: Keine Antwort erhalten</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Requested URL forces a redirection. Maximum number of redirections exceeded.</source>
        <translation>Angefragte URL erfordert eine Weiterleitung. Anzahl maximaler Weiterleitungen wurde jedoch überschritten.</translation>
    </message>
</context>
<context>
    <name>ito::FileSystemDockWidget</name>
    <message>
        <location filename="../widgets/fileSystemDockWidget.cpp" line="+140"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+213"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Directory &apos;%1&apos; does not exist!</source>
        <translation>Verzeichnis &apos;%1&apos; existiert nicht!</translation>
    </message>
    <message>
        <location line="-526"/>
        <source>Last used directories</source>
        <translation>Zuletzt verwendete Verzeichnisse</translation>
    </message>
    <message>
        <location line="+263"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+591"/>
        <location line="+13"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="-602"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>Open New Folder</source>
        <translation>Neues Verzeichnis öffnen</translation>
    </message>
    <message>
        <location line="-187"/>
        <source>file name filters (semicolon or space separated list)</source>
        <translation>Dateinamen der Filter (mit Semikolon oder Leerzeichen separierte Liste)</translation>
    </message>
    <message>
        <location line="+189"/>
        <source>Change To Parent Folder</source>
        <translation>Zum übergeordneten Verzeichnis wechseln</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy Path To Clipboard</source>
        <translation>Pfad in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Get Path From Clipboard</source>
        <translation>Pfad aus der Zwischenablage übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Execute File</source>
        <translation>Datei ausführen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Locate On Disk</source>
        <translation>Verzeichnis anzeigen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Create New Folder</source>
        <translation>Neuen Ordner erstellen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create New Python File</source>
        <translation>Neue Python-Datei erstellen</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>File System</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Select base directory</source>
        <translation>Aktuelles Stammverzeichnis auswählen</translation>
    </message>
    <message>
        <location line="+232"/>
        <source>the selected items</source>
        <translation>der ausgewählte Eintrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Do you really want to delete %1?</source>
        <translation>Soll &apos;%1&apos; wirklich gelöscht werden?</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Error while deleting &apos;%1&apos;!</source>
        <translation>Fehler beim Löschen von &apos;%1&apos;!</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>Error pasting or copying files</source>
        <translation>Fehler beim Einfügen oder Kopieren von Dateien</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>At least one of the selected items could not be moved or copied. Maybe an existing file should have been overwritten, but could not be deleted first.</source>
        <translation>Eines der ausgewählten Objekte kann nicht verschoben oder kopiert werden. Evtl. sollte eine vorhandene Datei überschrieben werden, die zuvor jedoch nicht gelöscht werden konnte.</translation>
    </message>
    <message>
        <source>Error pasting items</source>
        <translation type="vanished">Einfügen</translation>
    </message>
    <message>
        <source>The selected items could not be pasted from the clipboard. Maybe their URLs already exist</source>
        <translation type="vanished">Die markierten Objekte konnten nicht aus der Zwischenablage eingefügt werden. Vielleicht existieren die Objekte bereits</translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+24"/>
        <source>New Folder</source>
        <translation>Neuer Ordner</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Failed to create a new directory</source>
        <translation>Fehler beim Erstellen des neuen Ordners</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+28"/>
        <source>New Script</source>
        <translation>Neues Skript</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Failed to create a new script</source>
        <translation>Fehler beim Erstellen einer neuen Python-Datei</translation>
    </message>
</context>
<context>
    <name>ito::HelpDockWidget</name>
    <message>
        <location filename="../widgets/helpDockWidget.cpp" line="+69"/>
        <source>backwards</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>forwards</source>
        <translation>Vorwärts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>expand tree</source>
        <translation>Baum erweitern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>collapse tree</source>
        <translation>Baum reduzieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>reload database</source>
        <translation>Datenbank neu laden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>type text to filter the keywords in the tree</source>
        <translation>Text für die Filterung von Schlüsselwörtern im Baum eingeben</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>navigation</source>
        <translation>Navigation</translation>
    </message>
</context>
<context>
    <name>ito::HelpTreeDockWidget</name>
    <message>
        <location filename="../ui/helpTreeDockWidget.cpp" line="+142"/>
        <location line="+475"/>
        <location line="+165"/>
        <location line="+427"/>
        <source>Algorithms</source>
        <translation>Algorithmus</translation>
    </message>
    <message>
        <location line="-1028"/>
        <location line="+452"/>
        <location line="+160"/>
        <source>Widgets</source>
        <translation></translation>
    </message>
    <message>
        <location line="-573"/>
        <location line="+584"/>
        <location line="+409"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+4"/>
        <source>DataIO</source>
        <translation></translation>
    </message>
    <message>
        <location line="-998"/>
        <location line="+3"/>
        <location line="+585"/>
        <location line="+398"/>
        <source>Grabber</source>
        <translation></translation>
    </message>
    <message>
        <location line="-979"/>
        <location line="+3"/>
        <location line="+589"/>
        <location line="+391"/>
        <source>ADDA</source>
        <translation>ADDA Wandler</translation>
    </message>
    <message>
        <location line="-976"/>
        <location line="+3"/>
        <location line="+593"/>
        <location line="+384"/>
        <source>Raw IO</source>
        <translation></translation>
    </message>
    <message>
        <location line="-926"/>
        <location line="+553"/>
        <location line="+357"/>
        <source>Actuator</source>
        <translation>Motor</translation>
    </message>
    <message>
        <location line="-809"/>
        <source>Template Error: Parameters section is only defined by either the start or end tag.</source>
        <translation>Vorlagenfehler: Die Parametersektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Template Error: End tag of parameters section comes before start tag.</source>
        <translation>Vorlagenfehler: Der Endzeiger der Parametersektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+26"/>
        <source>Template Error: Returns section is only defined by either the start or end tag.</source>
        <translation>Vorlagenfehler: Die Rückgabesektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+26"/>
        <source>Template Error: End tag of returns section comes before start tag.</source>
        <translation>Vorlagenfehler: Der Endzeiger der Rückgabesektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Returns</source>
        <translation>Rückgabe</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Example</source>
        <translation>Beispiel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy example to clipboard</source>
        <translation>Beispiel in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+72"/>
        <location line="+161"/>
        <source>optional</source>
        <translation></translation>
    </message>
    <message>
        <location line="-186"/>
        <source>Unknown filter name &apos;%1&apos;</source>
        <translation>Unbekannter Filtername &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+83"/>
        <location line="+16"/>
        <source>This plugin contains the following</source>
        <translation>Dieses Plugin enthält folgende</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Unknown algorithm plugin with name &apos;%1&apos;</source>
        <translation>Unbekanntes Algorithmus-Plugin namens &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>dataIO</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>actuator</source>
        <translation>Motor</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>unknown type</source>
        <translation>Unbekannter Typ</translation>
    </message>
    <message>
        <location line="+163"/>
        <source>Template Error: %s section is only defined by either the start or end tag.</source>
        <translation>Vorlagenfehler: Die &quot;%s&quot;-Sektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Template Error: End tag of %s section comes before start tag.</source>
        <translation>Vorlagenfehler: Der Endzeiger der &quot;%s&quot;-Sektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Range: [%1,%2], Default: %3</source>
        <translation>Bereich: [%1, %2], Voreinstellung: %3</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Range: [%1:%2:%3], Default: %4</source>
        <translation>Bereich: [%1:%2:%3], Voreinstellung: %4</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Default: %1</source>
        <translation>Voreinstellung: %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Default: %1+%2i</source>
        <translation>Voreinstellung: %1+%2i</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Default: %1-%2i</source>
        <translation>Voreinstellung: %1-%2i</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>RegExp: &apos;%1&apos;</source>
        <translation>RegAusdr.: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>RegExp: [%1]</source>
        <translation>RegAusdr.: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>RegExp: &lt;no pattern given&gt;</source>
        <translation>RegAusdr.: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Match: &apos;%1&apos;</source>
        <translation>Treffer: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Match: [%1]</source>
        <translation>Treffer: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Match: &lt;no pattern given&gt;</source>
        <translation>Treffer: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wildcard: &apos;%1&apos;</source>
        <translation>Platzhalter: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wildcard: [%1]</source>
        <translation>Platzhalter: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Wildcard: &lt;no pattern given&gt;</source>
        <translation>Platzhalter: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Only plugin &apos;%1&apos; is allowed.</source>
        <translation>Nur ein Plugin vom Typ &apos;%1&apos; ist erlaubt.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Plugin of type &apos;%1&apos; are allowed.</source>
        <translation>Erlaubt sind Plugins vom Typ &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+462"/>
        <source>Database %s could not be opened</source>
        <translation>Datenbank &apos;%s&apos; konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Database %s could not be found</source>
        <translation>Datenbank &apos;%s&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Help database is loading...</source>
        <translation>Die Hilfe-Datenbank wird geladen...</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>No help database available! 
 go to Properties File -&gt; General -&gt; Helpviewer and check the selection</source>
        <translation>Keine Hilfe-Datenbank erreichbar!
Bitte unter Optionen -&gt; Allgemein -&gt; Hilfeanzeige die Einstellungen prüfen</translation>
    </message>
    <message>
        <location line="+703"/>
        <source>The protocol of the link is unknown. </source>
        <translation>Das Protokoll des Links ist unbekannt. </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to try with the external browser?</source>
        <translation>Soll versucht werden den Link mit dem Browser zu öffnen?</translation>
    </message>
</context>
<context>
    <name>ito::HelpViewer</name>
    <message>
        <location filename="../helpViewer/helpViewer.cpp" line="+67"/>
        <source>itom documentation</source>
        <translation>itom Dokumentation</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>content</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Search for:</source>
        <translation>Suchen nach:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>toolBar</source>
        <translation>Toolbar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Home</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Zoom in</source>
        <translation>Vergrößern</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Zoom out</source>
        <translation>Verkleinern</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Default zoom</source>
        <translation>Normale Ansicht</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Search text</source>
        <translation>Textsuche</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Go to</source>
        <translation>Gehe zu</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>qthelp://org.sphinx.itomdocumentation.%1/doc/index.html</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ito::IOHelper</name>
    <message>
        <location filename="../helper/IOHelper.cpp" line="+88"/>
        <location line="+880"/>
        <source>Multiple plugins</source>
        <translation type="unfinished">Multiple Plugins</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Multiple plugins provide methods to load the file of type &apos;%1&apos;. Please choose one.</source>
        <translation type="unfinished">Multiple Plugins unterstüzen Methoden um Dateien des Typs &apos;%1&apos; zu laden. Bitte einen auswählen.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>File &apos;%1&apos; could not be opened with registered external application</source>
        <translation>Die Datei &apos;%1&apos; konnte nicht mit der verknüpften externen Anwendung geöffnet werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File %1 can not be opened with this application</source>
        <translation>Die Datei &apos;%1&apos; kann mit dieser Anwendung nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Save selected variables as...</source>
        <translation>Speichern der markierten Variablen als...</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Import data</source>
        <translation>Daten importieren</translation>
    </message>
    <message>
        <location line="+455"/>
        <source>PolygonMesh and PointCloud not available since support of PointCloudLibrary is disabled in this version.</source>
        <translation>Polygonnetze und Punktewolken sind in dieser Version von PointCloudLibrary nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The algorithm interface is not supported</source>
        <translation>Die Algorithmusschnittstelle wird nicht unterstützt</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Timeout while sending values to python</source>
        <translation>Zeitüberschreitung beim Versuch Werte an Pyhton zu senden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>A timeout occurred while content of loaded file has been sent to python workspace</source>
        <translation>Beim Senden des Dateiinhalts der geladenen Datei an Python ist eine Zeitüberschreitung aufgetreten</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>An error occured while importing the loaded file into the python workspace.</source>
        <translation>Beim Import der geladenen Datei in den Python-Workspace ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A warning occured while importing the loaded file into the python workspace.</source>
        <translation>Beim Import der geladenen Datei in den Python-Workspace ist eine Warnung aufgetreten.</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Error while sending values to python</source>
        <translation>Fehler beim Senden der Werte an Python</translation>
    </message>
    <message>
        <location line="-385"/>
        <source>Please indicate a variable name for the dictionary in file &apos;%1&apos; 
(name must start with a letter followed by numbers or letters).</source>
        <translation>Bitte einen Variablenname für das &apos;Dictionary&apos; der Datei &apos;%1&apos; eingeben
(Der Name muss mit einem Buchstaben beginnen und darf nur Zahlen und Buchstaben enthalten).</translation>
    </message>
    <message>
        <location line="+390"/>
        <source>Warning while sending values to python</source>
        <translation>Warnung beim Senden von Werten an Python</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>AlgoInterfaceValidator not available.</source>
        <translation>&apos;AlgoInterfaceValidator&apos; ist nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>AddInManager or PythonEngine not available</source>
        <translation>&apos;AddInManager&apos; oder &apos;PythonEngine&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>There is no plugin interface able to save the requested file type</source>
        <translation>Es gibt keine Plugin-Schnittstelle um diesen Dateityp zu speichern</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Multiple plugins provide methods to save the file of type &apos;%1&apos;. Please choose one.</source>
        <translation type="unfinished">Diverse Plugins unterstützen Methoden um Dateien des Typs &apos;%1&apos; zu speichern. Bitte eines wählen.</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>AlgoInterfaceValidator not available</source>
        <translation>&apos;AlgoInterfaceValidato&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>AddInManager not available</source>
        <translation>Der &apos;AddInManager&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Python Scripts (*.py)</source>
        <translation>Python-Skripte (*.py)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>itom Data Collection (*.idc)</source>
        <translation>itom Datencontainer (*.idc)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Itom Data Collection (*.idc)</source>
        <translation>itom Datencontainer (*.idc)</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+4"/>
        <source>Matlab Matrix (*.mat)</source>
        <translation>Matlab-Matrizen (*.mat)</translation>
    </message>
    <message>
        <location line="-1041"/>
        <source>File %1 does not exist</source>
        <translation>Die Datei &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+199"/>
        <location line="+68"/>
        <location line="+145"/>
        <source>Python engine not available</source>
        <translation type="unfinished">Die Python-Engine ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-208"/>
        <location line="+68"/>
        <source>Variables cannot be exported since python is busy right now</source>
        <translation type="unfinished">Variablen können nicht exportiert werden während Python ausgeführt wird</translation>
    </message>
    <message>
        <location line="-58"/>
        <source>Timeout while getting value from workspace</source>
        <translation>Zeitüberschreitung beim Lesen der Werte aus dem Workspace</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The number of values returned from workspace does not correspond to requested number</source>
        <translation>Die Anzahl der zurückgegebenen Werte aus dem Workspace entspricht nicht der angeforderten Anzahl</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+145"/>
        <source>File cannot be opened</source>
        <translation>Die Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="-114"/>
        <source>Timeout while pickling variables</source>
        <translation type="unfinished">Zeitüberschreitung beim &quot;Pickeln&quot; von Variablen</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Timeout while saving variables to matlab file</source>
        <translation type="unfinished">Zeitüberschreitung beim Speichern von Variablen in eine Matlab-Datei</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+192"/>
        <source>Suffix must be *.idc or *.mat</source>
        <translation>Dateiendung muss *.idc oder *.mat sein</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>File not found</source>
        <translation>Datei nicht gefunden</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Variables cannot be imported since python is busy right now</source>
        <translation type="unfinished">Variablen können nicht importiert werden während Python läuft</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Variable name of imported dictionary</source>
        <translation type="unfinished">Variablenname für das zu importierende &apos;Dictionary&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid variable name</source>
        <translation>Ungültiger Variablenname</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Timeout while unpickling variables</source>
        <translation type="unfinished">Zeitüberschreitung beim &quot;Unpickeln&quot; von Variablen</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Timeout while loading matlab variables</source>
        <translation type="unfinished">Zeitüberschreitung beim Laden von Matlab-Variablen</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Timeout while opening script</source>
        <translation type="unfinished">Zeitüberschreitung beim Öffnen eines Skripts</translation>
    </message>
    <message>
        <location line="+413"/>
        <source>Algorithm interface not supported</source>
        <translation>Die Algorithmusschnittstelle wird nicht unterstützt</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Error while getting mand and out parameters from algorithm interface</source>
        <translation>Fehler beim Lesen von Ausgabeparametern der Algorithmusschnittstelle</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>User Interfaces (*.ui)</source>
        <translation>Eigene Schnittstellen (*.ui)</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>All Files (*.*)</source>
        <translation>Alle Dateien (*.*)</translation>
    </message>
</context>
<context>
    <name>ito::ItomFileSystemModel</name>
    <message>
        <location filename="../models/itomFileSystemModel.cpp" line="+79"/>
        <source>file does not exist.</source>
        <translation type="unfinished">Die Datei existiert nicht.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The source file &apos;%s&apos; does not exist and can not be moved or pasted</source>
        <translation type="unfinished">Die Quelldatei &apos;%s&apos; ist nicht vorhanden und kann deshalb nicht verschoben oder eingefügt werden</translation>
    </message>
    <message>
        <location line="+23"/>
        <source> (copy)</source>
        <translation type="unfinished"> (Kopie)</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+40"/>
        <source>Destination already exists.</source>
        <translation type="unfinished">Datei bereits vorhanden.</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>The file &apos;%s&apos; already exists. Should it be overwritten?</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits. Soll diese überschrieben werden?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>- Copy</source>
        <translation type="unfinished">- Kopie</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The file &apos;%s&apos; already exists. Should it be overwritten by the new link?</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits. Soll diese überschrieben werden?</translation>
    </message>
</context>
<context>
    <name>ito::LastCommandDockWidget</name>
    <message>
        <location filename="../widgets/lastCommandDockWidget.cpp" line="+191"/>
        <source>Clear List</source>
        <translation>Liste löschen</translation>
    </message>
</context>
<context>
    <name>ito::MainApplication</name>
    <message>
        <location filename="../mainApplication.cpp" line="+253"/>
        <location line="+4"/>
        <source>Version %1
%2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>64 bit (x64)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>32 bit (x86)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+111"/>
        <source>load translations...</source>
        <translation>Übersetzungen werden geladen...</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>load themes and styles...</source>
        <translation>Themen und Stile werden geladen...</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>load process organizer...</source>
        <translation>&apos;Process Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>scan and load plugins...</source>
        <translation>Plugins werden gescannt und geladen...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start python...</source>
        <translation>Python wird gestartet...</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>load main window...</source>
        <translation>Hauptfenster wird geladen...</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>load ui organizer...</source>
        <translation>&apos;UI Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>scan and load designer widgets...</source>
        <translation>&apos;Designer Widgets&apos; werden gescannt und geladen...</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>load script editor organizer...</source>
        <translation>&apos;Script Editor Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>execute startup scripts...</source>
        <translation>Startskripts werden ausgeführt...</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>scan and run scripts in autostart folder...</source>
        <translation>Skripts im Autostartordner werden gescannt und ausgeführt...</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Do you really want to exit the application?</source>
        <translation>Soll itom wirklich beendet werden?</translation>
    </message>
</context>
<context>
    <name>ito::MainWindow</name>
    <message>
        <location filename="../widgets/mainWindow.cpp" line="+120"/>
        <source>itom</source>
        <translation>itom</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Breakpoints</source>
        <translation>Haltepunkte</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>File System</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>itom (x64)</source>
        <translation></translation>
    </message>
    <message>
        <source>Python Messages</source>
        <translation type="obsolete">Python-Meldungen</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Call Stack</source>
        <translation>Aufrufliste</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Global Variables</source>
        <translation>Globale Variablen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Local Variables</source>
        <translation>Lokale Variablen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Plugins</source>
        <translation></translation>
    </message>
    <message>
        <location line="+383"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>close all floatable figures</source>
        <translation type="obsolete">Alle freien Grafiken schließen</translation>
    </message>
    <message>
        <source>show all floatable figures</source>
        <translation type="obsolete">Alle freien Grafiken einblenden</translation>
    </message>
    <message>
        <source>minimize all floatable figures</source>
        <translation type="obsolete">Alle freien Grafiken minimieren</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Continue</source>
        <translation>Fortsetzen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Step</source>
        <translation>Einzelschritt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Step Over</source>
        <translation>Prozedurschritt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Step Out</source>
        <translation>Ausführen bis Rücksprung</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload Modules...</source>
        <translation>Module neu laden...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Autoreload Modules</source>
        <translation>Automatisch neugeladene Module</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Autoreload Before Script Execution</source>
        <translation>Automatisches Neuladen vor der Skriptausführung</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Autoreload Before Single Command</source>
        <translation>Automatisches Neuladen vor dem Einzelbefehl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Autoreload Before Events And Function Calls</source>
        <translation>Automatisches Neuladen vor den Ereignis- und Funktionsaufrufen</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Recently Used Files</source>
        <translation>Zuletzt verwendete Dateien</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Figure</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location line="+209"/>
        <source>No Opened Scripts</source>
        <translation>Kein geöffnetes Skript</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Python is being executed</source>
        <translation>Python wird ausgeführt</translation>
    </message>
    <message>
        <location line="+144"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <source>timeout while closing figures</source>
        <translation type="obsolete">Zeitüberschreitung beim Schließen der Grafiken</translation>
    </message>
    <message>
        <location line="+418"/>
        <source>One single menu element must be of type MENU [2]</source>
        <translation>Das Menüelement muss vom Typ &apos;menu&apos; sein [2]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Key must not be empty.</source>
        <translation>Der Schlüssel darf nicht leer sein.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid menu item type.</source>
        <translation>Ungültiger Typ von &apos;menu item&apos;.</translation>
    </message>
    <message>
        <location line="+450"/>
        <source>There is no python code associated with this action.</source>
        <translation>Für diese Komponente wurde kein Python-Code hinterlegt.</translation>
    </message>
    <message>
        <location line="-1491"/>
        <source>New Script...</source>
        <translation>Neues Skript...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open File...</source>
        <translation>Datei öffnen...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Properties...</source>
        <translation>Optionen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User Management...</source>
        <translation>Benutzerverwaltung...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>About Qt...</source>
        <translation>Über Qt...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>About itom...</source>
        <translation>Über itom...</translation>
    </message>
    <message>
        <source>Loaded plugins...</source>
        <translation type="vanished">Geladene Plugins...</translation>
    </message>
    <message>
        <location line="-454"/>
        <source>Command History</source>
        <translation>Befehlsliste</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Help Viewer</source>
        <translation type="unfinished">Hilfeanzeige</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Python could not be started. itom cannot be used in the desired way. 
Start itom again with the argument &apos;log&apos; and look-up the error message in the file itomlog.txt.</source>
        <translation>Python konnte nicht gestartet werden. itom kann ohne Python nicht genutzt werden.
Bitte itom mit dem Argument &apos;log&apos; erneut starten und die Fehlermeldungen in der Datei &apos;itomlog.txt&apos; auswerten.</translation>
    </message>
    <message>
        <location line="+368"/>
        <source>Loaded Plugins...</source>
        <translation>Geladene Plugins...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help...</source>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close All Floatable Figures</source>
        <translation>Alle ausgedockten Plots schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show All Floatable Figures</source>
        <translation>Alle ausgedockten Plots anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Minimize All Floatable Figures</source>
        <translation>Alle ausgedockten Plots minimieren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Script Reference</source>
        <translation>Skriptreferenz</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>UI Designer</source>
        <translation>UI-Designer</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>F11</source>
        <translation></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>F10</source>
        <translation></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Shift+F11</source>
        <translation></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Package Manager...</source>
        <translation>Python Paket-Manager...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Timer Manager...</source>
        <translation>Timer-Manager...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Tools</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>About</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Current Figures</source>
        <translation>Aktuelle Plots</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Reload Modules</source>
        <translation>Module neuladen</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>No Entries</source>
        <translation>Kein Eintrag</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+113"/>
        <source>Instance of UiOrganizer not available</source>
        <translation type="unfinished">Die Instanz des UiOrganizers ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-100"/>
        <source>No Figures Available</source>
        <translation type="unfinished">Kein Plot verfügbar</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Toolboxes</source>
        <translation>Toolboxen</translation>
    </message>
    <message>
        <location line="-160"/>
        <source>Script</source>
        <translation>Skript</translation>
    </message>
    <message>
        <location line="+1342"/>
        <source>The UI designer (Qt designer) could not be started (%1).</source>
        <translation>Der UI-Designer (QT-Designer) konnte nicht geöffnet werden (%1).</translation>
    </message>
    <message>
        <location line="-1319"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location line="-198"/>
        <source>Run Python Code In Debug Mode</source>
        <translation>Python-Code im Debug-Modus ausführen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set whether internal python code should be executed in debug mode</source>
        <translation>Interner Pyhton-Code wird im Debug-Modus ausgeführt</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shift+F5</source>
        <translation></translation>
    </message>
    <message>
        <location line="+412"/>
        <source>Ready</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location line="+198"/>
        <source>The file &apos;%s&apos; is not a valid help collection file or does not exist.</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; ist keine gültige &quot;help collection&quot;-Datei oder existiert nicht.</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Process Organizer could not be loaded</source>
        <translation type="unfinished">Der&quot; Process Organizer&quot; konnte nicht geladen werden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error when preparing help or showing assistant.</source>
        <translation>Fehler bei der Erstellung oder dem Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error while showing assistant.</source>
        <translation>Fehler beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning when preparing help or showing assistant.</source>
        <translation>Warnung bei der Erstellung oder dem Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while showing assistant.</source>
        <translation>Warnung beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+12"/>
        <location line="+12"/>
        <source>The UiOrganizer is not available</source>
        <translation type="unfinished">Der UiOrganizer ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>The help assistant could not be started.</source>
        <translation>Der Hilfeassistent konnte nicht gestartet werden.</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>The button &apos;%s&apos; of toolbar &apos;%s&apos; could not be found.</source>
        <translation type="unfinished">Der Button &apos;%s&apos; der Toolbar &apos;%s&apos; wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The toolbar &apos;%s&apos; could not be found.</source>
        <translation type="unfinished">Die Toolbar &apos;%s&apos; wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>The button (%i) could not be found.</source>
        <translation type="unfinished">Der Button (%i) wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="+142"/>
        <location line="+4"/>
        <source>Add menu element</source>
        <translation>Menüpunkt hinzufügen</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+46"/>
        <source>A user-defined menu with the key sequence &apos;%s&apos; could not be found</source>
        <translation type="unfinished">Ein benutzerdefiniertes Menü mit der Key-Sequenz &apos;%s&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+64"/>
        <source>Remove menu element</source>
        <translation>Menüelement löschen</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>A user-defined menu with the handle &apos;%i&apos; could not be found</source>
        <translation type="unfinished">Ein benutzerdefiniertes Menü mit dem Handle &apos;%i&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+174"/>
        <source>Current Directory: %1</source>
        <translation>Aktuelles Verzeichnis: %1</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Python is not available. This action cannot be executed.</source>
        <translation>Python ist nicht verfügbar. Diese Anwendung kann nicht ausgeführt werden.</translation>
    </message>
</context>
<context>
    <name>ito::PaletteOrganizer</name>
    <message>
        <location filename="../organizer/paletteOrganizer.cpp" line="+553"/>
        <source>Settings do not contain a color palette entry for the palette name &apos;%s&apos;</source>
        <translation>Es existiert kein Farbpaletteneintrag mit dem Palettennamen &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>Palette %1 has a restricted access.</source>
        <translation>Die Palette &apos;%1&apos; hat eine Zugangsbeschränkung.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Palette %1 has a write protection.</source>
        <translation>Die Palette &apos;%1&apos; ist schreibgeschützt.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Palette %1 not found within palette list</source>
        <translation>Die Palette &apos;%1&apos; wurde in der Palettenliste nicht gefunden</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Destination vector not initialized</source>
        <translation>Der Zielvektor wurde nicht initialisiert</translation>
    </message>
</context>
<context>
    <name>ito::ParamInputDialog</name>
    <message>
        <location filename="../ui/paramInputDialog.cpp" line="+142"/>
        <source>IntArray</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>DoubleArray</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>CharArray</source>
        <translation></translation>
    </message>
    <message>
        <location line="+227"/>
        <source>The number of value does not match the step size</source>
        <translation>Der angegebene Wert entspricht nicht den möglichen Werten für die Schrittgröße</translation>
    </message>
</context>
<context>
    <name>ito::ParamInputParser</name>
    <message>
        <location filename="../ui/paramInputParser.cpp" line="+65"/>
        <location line="+131"/>
        <location line="+81"/>
        <source>Canvas widget does not exist any more</source>
        <translation>Container existiert nicht länger</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>[no description]</source>
        <translation>[keine Beschreibung]</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>[Integer]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[Char]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[Double]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[String]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[IntArray]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[DoubleArray]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[CharArray]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[HW-Instance]</source>
        <translation>[HW-Instanz]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[unknown]</source>
        <translation>[unbekannt]</translation>
    </message>
    <message>
        <location line="+1"/>
        <source> - - error - - </source>
        <translation> - - FEHLER - - </translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+81"/>
        <source>QT error: Grid layout could not be identified</source>
        <translation>QT-Fehler: Grid Layout kann nicht identifiziert werden</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>The parameter &apos;%1&apos; is invalid.</source>
        <translation>Der Parameter &apos;%1&apos; ist ungültig.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid input</source>
        <translation>Ungültiger Input</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>checked: 1, unchecked: 0</source>
        <translation>ausgewählt: 1, nicht ausgewählt: 0</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+27"/>
        <location line="+29"/>
        <source>min: %1, max: %2, step: %3</source>
        <translation>min: %1, max: %2, Schritt: %3</translation>
    </message>
    <message>
        <location line="-49"/>
        <location line="+27"/>
        <location line="+33"/>
        <source>unlimited</source>
        <translation>Unlimitiert</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>min: %1, max: %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1 [Wildcard]</source>
        <translation>%1 [Platzhalter]</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 [Regular Expression]</source>
        <translation>%1 [Regulärer Ausdruck]</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+394"/>
        <source>[None]</source>
        <translation></translation>
    </message>
    <message>
        <location line="-313"/>
        <location line="+23"/>
        <source>Qt error: Spin box widget could not be found</source>
        <translation>Qt-Fehler: Spinbox-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Qt error: Double spin box widget could not be found</source>
        <translation>Qt-Fehler: Double-Spinbox-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Qt error: String input widget could not be found</source>
        <translation>Qt-Fehler: String-Eingabe-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Qt error: IntArray widget could not be found</source>
        <translation>Qt-Fehler: IntArray-Eingabe-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+108"/>
        <source>Invalid integer list of parameter &apos;%1&apos;: Value &apos;%2&apos; at position %3 is no integer number.</source>
        <translation>Ungültige Integer-Liste in Parameter &apos;%1&apos;: Wert &apos;%2&apos; an Position %3 ist keine Ganzzahl.</translation>
    </message>
    <message>
        <location line="-75"/>
        <source>Qt error: DoubleArray widget could not be found</source>
        <translation>Qt-Fehler: DoubleArray-Eingabe-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Invalid double list of parameter &apos;%1&apos;: Value &apos;%2&apos; at position %3 is no double number.</source>
        <translation>Ungültige Double-Liste in Parameter &apos;%1&apos;: Wert &apos;%2&apos; an Position %3 ist keine Kommazahl.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Qt error: CharArray widget could not be found</source>
        <translation>Qt-Fehler: CharArray-Eingabe-Widget wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>%1, Identifier: %2</source>
        <translation type="unfinished">%1, Indikator: %2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1, ID: %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Invalid integer list: Value &apos;%1&apos; at position %2 is no integer number.</source>
        <translation>Ungültige Integer-Liste: Wert &apos;%1&apos; an Position %2 ist keine Ganzzahl.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Invalid double list: Value &apos;%1&apos; at position %2 is no double number.</source>
        <translation>Ungültige Double-Liste: Wert &apos;%1&apos; an Position %2 ist keine Kommazahl.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unknown paramType: %1</source>
        <translation>Unbekannter Parametertyp: %1</translation>
    </message>
</context>
<context>
    <name>ito::PipManager</name>
    <message>
        <location filename="../models/pipManager.cpp" line="+50"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Location</source>
        <translation>Speicherort</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Requires</source>
        <translation>Erforderlich</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Updates</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Summary</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Homepage</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>The itom subdirectory of Python &apos;%s&apos; is not existing.
Please change setting in the property dialog of itom.</source>
        <translation type="unfinished">Das itom-Unterverzeichnis von Python &apos;%s&apos; existiert nicht.
Bitte die Einstellungen unter &apos;Optionen&apos; ändern.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Settings value Python::pyHome has not been set as Python Home directory since it does not exist:  %s</source>
        <translation type="unfinished">Der unter &apos;Python::pyHome&apos; eingestellte Wert wurde nicht als Python-Verzeichnis gesetzt, da dieses Verzeichnis nicht existiert: %s</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>The home directory of Python is currently set to the non-existing directory &apos;%s&apos;
Python cannot be started. Please set either the environment variable PYTHONHOME to the base directory of python 
or correct the base directory in the property dialog of itom.</source>
        <translation type="unfinished">Das Stammverzeichnis von Python ist aktuell auf ein nicht existierendes Verzeichnis gesetzt (&apos;%s&apos;)
Python kann nicht gestartet werden. Bitte entweder im System die Umgebungsvariable &apos;PYTHONHOME&apos; mit dem Stammverzeichnis von Python setzen
oder in itom unter Optionen das Python-Verzeichnis korrigieren.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The built-in library path of Python could not be found. The current home directory is &apos;%s&apos;
Python cannot be started. Please set either the environment variable PYTHONHOME to the base directory of python 
or correct the base directory in the preferences dialog of itom.</source>
        <translation type="unfinished">Das Verzeichnis zur &apos;built-in library&apos; von Python wurde nicht gefunden. Der aktuelle Pfad des Python-Stammverzeichnisses lautet &apos;%s&apos;
Python kann nicht gestartet werden. Bitte entweder im System die Umgebungsvariable &apos;PYTHONHOME&apos; mit dem Stammverzeichnis von Python setzen
oder in itom unter Optionen das Python-Verzeichnis korrigieren.</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>up to date</source>
        <translation>aktuell</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>new version %1 available</source>
        <translation>neue Version %1 verfügbar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location line="+360"/>
        <source>Could not start python pip
</source>
        <translation>Python-Pip kann nicht gestartet werden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>An error occurred when attempting to read from the process.
</source>
        <translation>Beim Versuch auf den Prozess zuzugreifen ist ein Fehler aufgetreten.
</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>other error</source>
        <translation>Es ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Python pip crashed during execution
</source>
        <translation>Python-Pip ist bei der Ausführung abgestürzt</translation>
    </message>
</context>
<context>
    <name>ito::PyGotoAssignmentMode</name>
    <message>
        <location filename="../codeEditor/modes/pyGotoAssignment.cpp" line="+79"/>
        <source>Go To Definition</source>
        <translation type="unfinished">Gehe zu Definition</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Go To Assignment</source>
        <translation type="unfinished">Gehe zu Zuweisung</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Go To Assignment (Follow Imports)</source>
        <translation type="unfinished">Gehe zu Zuweisung (auch importierte Module)</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+243"/>
        <source>No definition found</source>
        <translation type="unfinished">Es wurde keine Definition gefunden</translation>
    </message>
    <message>
        <location line="-243"/>
        <location line="+243"/>
        <source>No definition could be found.</source>
        <translation type="unfinished">Es wurde keine Definition gefunden.</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Choose a definition</source>
        <translation type="unfinished">Eine Definition auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose the definition you want to go to:</source>
        <translation type="unfinished">Die Definition auswählen, die angezeigt werden soll:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Source of definition not reachable</source>
        <translation type="unfinished">Der Quellcode dieser Definition ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The source of this definition is not reachable.</source>
        <translation type="unfinished">Der Quellcode dieser Definition ist nicht verfügbar.</translation>
    </message>
</context>
<context>
    <name>ito::PythonEngine</name>
    <message>
        <location filename="../python/pythonEngine.cpp" line="+1041"/>
        <source>Python not initialized</source>
        <translation type="unfinished">Python ist nicht inizialisiert</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Qt text encoding not compatible to python. Python encoding is set to latin 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Qt text encoding %1 not compatible to python. Python encoding is set to latin 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+169"/>
        <source>An automatic reload cannot be executed since auto reloader is not enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>An automatic reload cannot be executed since module &apos;autoreload&apos; could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+570"/>
        <source>syntax error</source>
        <translation type="unfinished">Syntaxfehler</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>exiting desired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+298"/>
        <location line="+122"/>
        <location line="+115"/>
        <source>Error while clearing all breakpoints in itoDebugger.</source>
        <translation type="unfinished">Fehler beim Löschen aller Haltepunkte im ITO-Debugger.</translation>
    </message>
    <message>
        <location line="+2689"/>
        <source>It is not allowed to save a variable in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Variables can not be saved since dictionary is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+87"/>
        <location line="+1062"/>
        <location line="+99"/>
        <source>It is not allowed to pickle a variable in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1127"/>
        <location line="+33"/>
        <source>Could not save dataObject since it is not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unsupported data type to save to matlab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error converting object to Python object. Save to matlab not possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>It is not allowed to load matlab variables in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Variables can not be load since dictionary is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+139"/>
        <location line="+103"/>
        <location line="+96"/>
        <source>Values cannot be saved since workspace dictionary not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-28"/>
        <source>The number of names and values must be equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-4855"/>
        <source>The itom subdirectory of Python &apos;%s&apos; is not existing.
Please change setting in the property dialog of itom.</source>
        <translation type="unfinished">Das itom-Unterverzeichnis von Python &apos;%s&apos; existiert nicht.
Bitte die Einstellungen unter &apos;Optionen&apos; ändern.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Settings value Python::pyHome has not been set as Python Home directory since it does not exist:  %s</source>
        <translation type="unfinished">Der unter &apos;Python::pyHome&apos; eingestellte Wert wurde nicht als Python-Verzeichnis gesetzt, da dieses Verzeichnis nicht existiert: %s</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>The home directory of Python is currently set to the non-existing directory &apos;%s&apos;
Python cannot be started. Please set either the environment variable PYTHONHOME to the base directory of python 
or correct the base directory in the property dialog of itom.</source>
        <translation type="unfinished">Das Stammverzeichnis von Python ist aktuell auf ein nicht existierendes Verzeichnis gesetzt (&apos;%s&apos;)
Python kann nicht gestartet werden. Bitte entweder im System die Umgebungsvariable &apos;PYTHONHOME&apos; mit dem Stammverzeichnis von Python setzen
oder in itom unter Optionen das Python-Verzeichnis korrigieren.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The built-in library path of Python could not be found. The current home directory is &apos;%s&apos;
Python cannot be started. Please set either the environment variable PYTHONHOME to the base directory of python 
or correct the base directory in the preferences dialog of itom.</source>
        <translation type="unfinished">Das Verzeichnis zur &apos;built-in library&apos; von Python wurde nicht gefunden. Der aktuelle Pfad des Python-Stammverzeichnisses lautet &apos;%s&apos;
Python kann nicht gestartet werden. Bitte entweder im System die Umgebungsvariable &apos;PYTHONHOME&apos; mit dem Stammverzeichnis von Python setzen
oder in itom unter Optionen das Python-Verzeichnis korrigieren.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Numpy.core.multiarray failed to import. Please verify that you have numpy 1.6 or higher installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Numpy.core.multiarray failed to import. Please verify that you have numpy 1.6 or higher installed.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Error importing sys in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error importing itom in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error redirecting stdout in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error redirecting stderr in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error redirecting stdin in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+257"/>
        <source>The module itoFunctions could not be loaded. Make sure that the script itoFunctions.py is available in the itom root directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The module itoDebugger could not be loaded. Make sure that the script itoDebugger.py is available in the itom root directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The class itoDebugger in the module itoDebugger could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The module &apos;autoreload&apos; could not be loaded. Make sure that the script autoreload.py is available in the itom-packages directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Deadlock in python setup.</source>
        <translation type="unfinished">Deadlock in Python.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Syntax check not possible since package pyflakes missing. Install it or disable the syntax check in the properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+503"/>
        <source>Main dictionary is empty</source>
        <translation type="unfinished">Hauptwörterbuch ist leer</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Error while evaluating python string.</source>
        <translation type="unfinished">Fehler beim Evaluieren eines Python-Strings.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>File does not exist</source>
        <translation type="unfinished">Datei existiert nicht</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File could not be opened in readonly-mode</source>
        <translation type="unfinished">Datei konnte nicht im &quot;ReadOnly&quot;-Modus geöffnet werden</translation>
    </message>
    <message>
        <location line="+1048"/>
        <source>Adding breakpoint to file &apos;%s&apos;, line %i failed in Python debugger (invalid breakpoint id).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Adding breakpoint to file &apos;%s&apos;, line %i in Python debugger returned unknown error string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+67"/>
        <source>Debugger not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Exception raised while editing breakpoint in debugger.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Editing breakpoint (file &apos;%s&apos;, line %i) in Python debugger returned error code %i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Editing breakpoint (file &apos;%s&apos;, line %i) in Python debugger returned unknown error string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Breakpoint in file &apos;%s&apos;, line %i could not be edited since it has no valid Python breakpoint number (maybe a comment or blank line in script)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Exception raised while clearing breakpoint in debugger.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Deleting breakpoint in Python debugger returned error code %i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Deleting breakpoint in Python debugger returned unknown error string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2556"/>
        <source>Error while transforming value &apos;%s&apos; to PyObject*.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+71"/>
        <source>It is not allowed to load variables in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Values cannot be obtained since workspace dictionary not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Item &apos;%1&apos; does not exist in workspace.</source>
        <translation type="unfinished">Das Objekt &apos;%1&apos; existiert nicht im Workspace.</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>Variable name &apos;%1&apos; already exists in dictionary</source>
        <translation type="unfinished">Der Variablenname &apos;%1&apos; existiert bereis im Dictionary</translation>
    </message>
    <message>
        <location line="+120"/>
        <location line="+75"/>
        <source>It is not allowed to get modules if python is currently executed</source>
        <translation type="unfinished">Es ist nicht möglich Python-Module anzufordern während Python ausgeführt wird</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+21"/>
        <location line="+208"/>
        <location line="+75"/>
        <source>The script itomFunctions.py is not available</source>
        <translation type="unfinished">Das Skript &apos;itomFunctions.py&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>Error while loading the modules</source>
        <translation type="unfinished">Fehler beim Laden der Module</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Error while reloading the modules</source>
        <translation type="unfinished">Fehler beim erneuten Laden der Module</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Variables can not be pickled since dictionary is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Could not pickle since value is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+16"/>
        <source>Could not pickle dataObject since it is not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unsupported data type to pickle.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error converting object to Python object. No pickle possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+212"/>
        <source>MainModule is empty or cannot be accessed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-103"/>
        <source>It is not allowed to unpickle a data collection in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Variables can not be unpickled since dictionary is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Unpickling error. This file contains no dictionary as base element.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1566"/>
        <source>Given value is empty. No save to matlab possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2711"/>
        <source>Auto completion, calltips, goto definition... not possible, since the package &apos;jedi&apos; could not be loaded (Python packages &apos;jedi&apos; and &apos;parso&apos; are required for this feature).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+529"/>
        <location line="+2"/>
        <source>Adding breakpoint to file &apos;%1&apos;, line %2 failed in Python debugger.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2214"/>
        <source>could not save dataObject since it is not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+220"/>
        <location line="+103"/>
        <source>It is not allowed to check names of variables in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+138"/>
        <source>Function &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Method &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Type or class &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Module &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+78"/>
        <source>The number of names and types must be equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+143"/>
        <source>It is not allowed to register an AddIn-instance in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+56"/>
        <source>Dictionary is not available</source>
        <translation type="unfinished">Wörterbuch ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>No instance of python class dataIO could be created</source>
        <translation type="unfinished">Es konnte keine Instanz der Klasse &apos;DataID&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>No instance of python class actuator could be created</source>
        <translation type="unfinished">Es konnte keine Instanz der Klasse &apos;Motor&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>AddIn must be of type dataIO or actuator</source>
        <translation type="unfinished">AddIn muss vom Typ &apos;DataIO&apos; oder &apos;Motor&apos; sein</translation>
    </message>
    <message>
        <location line="+520"/>
        <source>No more memory available during pickling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The exception &apos;%s&apos; has been thrown during pickling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+5"/>
        <source>Pickle error. An unspecified exception has been thrown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+195"/>
        <source>No more memory available during unpickling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The exception &apos;%s&apos; has been thrown during unpickling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+5"/>
        <source>Unpickling error. An unspecified exception has been thrown.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::PythonMessageDockWidget</name>
    <message>
        <source>Clear List</source>
        <translation type="obsolete">Liste löschen</translation>
    </message>
</context>
<context>
    <name>ito::QsciApiManager</name>
    <message>
        <source>The python syntax documents have changed. The API has been updated.</source>
        <translation type="vanished">Die Python Syntaxliste hat sich geändert. Die API wurde aktuallisiert.</translation>
    </message>
    <message>
        <source>The generation of the python syntax API has been cancelled.</source>
        <translation type="vanished">Die Erstellung der Python-Syntax-API wurde abgebrochen.</translation>
    </message>
    <message>
        <source>The python syntax documents have changed. The API is being updated...</source>
        <translation type="vanished">Die Python Syntaxliste hat sich geändert. Die API wird aktuallisiert...</translation>
    </message>
</context>
<context>
    <name>ito::ScriptDockWidget</name>
    <message>
        <location filename="../widgets/scriptDockWidget.cpp" line="+649"/>
        <source>The following files have been changed and should be safed:</source>
        <translation>Folgende Dateien wurden geändert und sollten gespeichert werden:</translation>
    </message>
    <message>
        <location line="+622"/>
        <source>Ctrl+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+Shift+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>F5</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+6"/>
        <source>F6</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F11</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F10</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F11</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>F3</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+918"/>
        <source>&apos;%1&apos; was not found</source>
        <translation>&apos;%1&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+89"/>
        <location line="+4"/>
        <source>Find And Replace</source>
        <translation>Suchen und Ersetzen</translation>
    </message>
    <message>
        <source>%1 occurrence(s) was replaced</source>
        <translation type="vanished">%1 Vorkommen ersetzt</translation>
    </message>
    <message>
        <location line="-1008"/>
        <source>Ctrl+H</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Shift+F5</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Ctrl+B</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="-756"/>
        <source>File Open</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>File not found</source>
        <translation>Datei nicht gefunden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file %1 could not be found</source>
        <translation>Die Datei &apos;%1&apos; konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid file format</source>
        <translation>Unzulässiges Dateiformat</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file %1 is no python macro</source>
        <translation>Die Datei &apos;%1&apos;  ist kein Python-Makro</translation>
    </message>
    <message>
        <location line="+609"/>
        <source>Move Left</source>
        <translation>Nach Links verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move Right</source>
        <translation>Nach rechts verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move First</source>
        <translation>An den Anfang verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move Last</source>
        <translation>Ans Ende verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close Others</source>
        <translation>Alle anderen schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close All</source>
        <translation>Alles schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dock</source>
        <translation>Andocken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Undock</source>
        <translation>Lösen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save As...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>Alles speichern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Redo</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Comment</source>
        <translation>Kommentieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Uncomment</source>
        <translation>Auskommentieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Indent</source>
        <translation>Einrücken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Tab</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unindent</source>
        <translation>Ausrücken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shift+Tab</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run</source>
        <translation>Start</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run Selection</source>
        <translation>Auswahl starten</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F9</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>Fortsetzen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step</source>
        <translation>Einzelschritt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step Over</source>
        <translation>Prozedurschritt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Step Out</source>
        <translation>Ausführen bis Rücksprung</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <source>Quick Search...</source>
        <translation>Schnellsuche...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find And Replace...</source>
        <translation>Suchen und Ersetzen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Icon &amp;Browser...</source>
        <translation>Icon &amp;suchen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+G</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Goto...</source>
        <translation>Gehe zu...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Toggle Bookmark</source>
        <translation>Lesezeichen ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Next Bookmark</source>
        <translation>&amp;Nächstes Lesezeichen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Previous Bookmark</source>
        <translation>&amp;Vorheriges Lesezeichen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Clear All Bookmarks</source>
        <translation>Alle Lesezeichen &amp;löschen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Insert Codec...</source>
        <translation>&amp;Kodierung einfügen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Filename</source>
        <translation>Dateiname kopieren</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>No Entries</source>
        <translation>Kein Eintrag</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Recently Used Files</source>
        <translation>Zuletzt verwendete Dateien</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Bookmark</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Script</source>
        <translation>&amp;Skript</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>File Toolbar</source>
        <translation>Symbolleiste Datei</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edit Toolbar</source>
        <translation>Symbolleiste Bearbeiten</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Script Toolbar</source>
        <translation>Symbolleiste Skript</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bookmark Toolbar</source>
        <translation>Symbolleiste Lesezeichen</translation>
    </message>
    <message>
        <location line="+754"/>
        <source>One occurrence was replaced</source>
        <translation>Es wurde ein Element ersetzt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 occurrences were replaced</source>
        <translation>Es wurden %1 Elemente ersetzt</translation>
    </message>
</context>
<context>
    <name>ito::ScriptEditorOrganizer</name>
    <message>
        <location filename="../organizer/scriptEditorOrganizer.cpp" line="+302"/>
        <source>Script Editor</source>
        <translation>Skript-Editor</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>The following files have been changed and should be safed:</source>
        <translation>Folgende Dateien wurden geändert und sollten gespeichert werden:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>remember selection for the next time (can be reverted in property dialog)</source>
        <translation>Auswahl für das nächste Mal merken (Einstellung kann bei den Optionen geändert werden)</translation>
    </message>
</context>
<context>
    <name>ito::ScriptEditorWidget</name>
    <message>
        <location filename="../widgets/scriptEditorWidget.cpp" line="+752"/>
        <source>Choose an encoding of the file which is added to the first line of the script</source>
        <translation>Eine Kodierung für die erste Zeile im Skript auswählen</translation>
    </message>
    <message>
        <location line="+899"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>There is nothing to print</source>
        <translation>Es gibt nichts zu drucken</translation>
    </message>
    <message>
        <source>&amp;Toggle Bookmark</source>
        <translation type="vanished">Lesezeichen ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <source>Next Bookmark</source>
        <translation type="vanished">Nächstes Lesezeichen</translation>
    </message>
    <message>
        <source>Previous Bookmark</source>
        <translation type="vanished">Vorheriges Lesezeichen</translation>
    </message>
    <message>
        <source>Clear All Bookmarks</source>
        <translation type="vanished">Alle Lesezeichen löschen</translation>
    </message>
    <message>
        <source>&amp;Toggle Breakpoint</source>
        <translation type="vanished">Haltepunkt ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <source>&amp;Disable Breakpoint</source>
        <translation type="vanished">Haltepunkt &amp;deaktivieren</translation>
    </message>
    <message>
        <source>&amp;Edit Condition</source>
        <translation type="vanished">Bedingungen &amp;bearbeiten</translation>
    </message>
    <message>
        <source>&amp;Next Breakpoint</source>
        <translation type="vanished">&amp;Nächster Haltepunkt</translation>
    </message>
    <message>
        <source>&amp;Previous Breakpoint</source>
        <translation type="vanished">&amp;Vorheriger Haltepunkt</translation>
    </message>
    <message>
        <source>&amp;Delete All Breakpoints</source>
        <translation type="vanished">Alle Haltepunkte &amp;löschen</translation>
    </message>
    <message>
        <source>&amp;Cut</source>
        <translation type="vanished">&amp;Ausschneiden</translation>
    </message>
    <message>
        <source>Cop&amp;y</source>
        <translation type="vanished">&amp;Kopieren</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">&amp;Einfügen</translation>
    </message>
    <message>
        <source>&amp;Indent</source>
        <translation type="vanished">Zeileneinzug ver&amp;größern</translation>
    </message>
    <message>
        <source>&amp;Unindent</source>
        <translation type="vanished">Zeileneinzug ver&amp;kleinern</translation>
    </message>
    <message>
        <source>&amp;Comment</source>
        <translation type="vanished">&amp;Kommentieren</translation>
    </message>
    <message>
        <source>Unc&amp;omment</source>
        <translation type="vanished">Kommentierung &amp;aufheben</translation>
    </message>
    <message>
        <source>&amp;Run Script</source>
        <translation type="vanished">Skript &amp;starten</translation>
    </message>
    <message>
        <source>Run &amp;Selection</source>
        <translation type="vanished">Aus&amp;wahl starten</translation>
    </message>
    <message>
        <source>&amp;Debug Script</source>
        <translation type="vanished">Skript im &amp;Debug-Modus starten</translation>
    </message>
    <message>
        <source>Sto&amp;p Script</source>
        <translation type="vanished">Skript sto&amp;ppen</translation>
    </message>
    <message>
        <location line="-1353"/>
        <source>Cut</source>
        <translation type="unfinished">Ausschneiden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation type="unfinished">Einfügen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Indent</source>
        <translation type="unfinished">Einrücken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Tab</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unindent</source>
        <translation type="unfinished">Ausrücken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shift+Tab</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ctrl+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncomment</source>
        <translation type="unfinished">Auskommentieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ctrl+Shift+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Run Script</source>
        <translation type="unfinished">Skript starten</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F5</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Run Selection</source>
        <translation type="unfinished">Auswahl starten</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F9</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Debug Script</source>
        <translation type="unfinished">Skript im Debug-Modus starten</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F6</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop Script</source>
        <translation>Skript stoppen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shift+F5</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Folding</source>
        <translation>Gliederung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fold/Unfold &amp;Toplevel</source>
        <translation>&amp;Toplevel reduzieren/erweitern</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fold/Unfold &amp;All</source>
        <translation>&amp;Alles reduzieren/erweitern</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Unfold All</source>
        <translation>Alle Gliederungen &amp;erweitern</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Fold All</source>
        <translation type="unfinished">Alles er&amp;weitern</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Insert Codec...</source>
        <translation>&amp;Kodierung einfügen...</translation>
    </message>
    <message>
        <location line="+1608"/>
        <source>The file &apos;%1&apos; has been modified by another program.</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; wurde durch ein anderes Programm geändert.</translation>
    </message>
    <message>
        <source>&amp;Enable Breakpoint</source>
        <translation type="vanished">Haltepunkt &amp;aktivieren</translation>
    </message>
    <message>
        <location line="-1180"/>
        <source>Insert Codec</source>
        <translation>Kodierung einfügen</translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+84"/>
        <location line="+49"/>
        <source>Unsaved Changes</source>
        <translation>Ungespeicherte Änderungen</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+133"/>
        <source>There are unsaved changes in the current document. Do you want to save it first?</source>
        <translation>Es gibt noch ungespeicherte Änderungen im aktuellen Dokument. Sollen diese zuerst gespeichert werden?</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Error while opening file</source>
        <translation>Fehler beim Öffnen der Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>File %1 could not be loaded</source>
        <translation>Die Datei %1 konnte nicht geladen werden</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>There are unsaved changes in the document &apos;%1&apos;. Do you want to save it first?</source>
        <translation>Im Dokument &apos;%1&apos; gibt es noch ungespeicherte Änderungen. Sollen diese zuerst gespeichert werden?</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+64"/>
        <source>Error while accessing file</source>
        <translation>Fehler beim Zugriff auf die Datei</translation>
    </message>
    <message>
        <location line="-64"/>
        <location line="+64"/>
        <source>File %1 could not be accessed</source>
        <translation>Auf die Datei %1 konnte nicht zugegriffen werden</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Save As...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location line="+719"/>
        <source>Unnamed</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <location line="+239"/>
        <source>The file &apos;%1&apos; does not exist any more.</source>
        <translation>Die Datei &apos;%1&apos; existiert nicht mehr.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keep this file in editor?</source>
        <translation>Diese Datei im Editor belassen?</translation>
    </message>
    <message>
        <source>The file &apos;%1&apos; has been modified by another programm.</source>
        <translation type="vanished">Die Datei &apos;%1&apos; wurde von einem anderen Programm geändert.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Do you want to reload it?</source>
        <translation>Soll diese neu geladen werden?</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>{Global Scope}</source>
        <translation>{Globaler Gültigkeitsbereich}</translation>
    </message>
    <message>
        <location filename="../widgets/scriptEditorWidget.h" line="+92"/>
        <source>Untitled%1</source>
        <translation>Unbenannt%1</translation>
    </message>
</context>
<context>
    <name>ito::UiOrganizer</name>
    <message>
        <location filename="../organizer/uiOrganizer.cpp" line="+304"/>
        <source>the plugin did not return a valid widget pointer.</source>
        <translation type="unfinished">Das Plugin gibt keinen gültigen Widget-Pointer zurück.</translation>
    </message>
    <message>
        <location line="+265"/>
        <source>plugin with name &apos;%1&apos; could be found.</source>
        <translation type="unfinished">Das Plugin namens &apos;%1&apos; wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>figHandle %i is no handle for a figure window.</source>
        <translation type="unfinished">&quot;figHandle&quot; &apos;%i&apos; ist kein Handle eines Grafikfensters.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>No internal dialog or window with name &apos;%1&apos; could be found.</source>
        <translation type="unfinished">Es wurde kein interner Dialog oder Fenster namens &apos;%1&apos; gefunden.</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>ui-file &apos;%1&apos; could not be correctly parsed.</source>
        <translation type="unfinished">Die UI-Datei &apos;%1&apos; wurde nicht korrekt geparst.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>filename &apos;%1&apos; does not exist</source>
        <translation type="unfinished">Dateiname &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="-313"/>
        <source>dialog could not be created</source>
        <translation type="unfinished">Der Dialog kann nicht erstellt werden</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>dockWidgetArea is invalid</source>
        <translation type="unfinished">Das &apos;dockWidgetArea&apos; ist ungültig</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A widget inherited from QDialog cannot be docked into the main window</source>
        <translation type="unfinished">Ein Widget, welches von QDialog abgeleitet wurde, kann nicht an das Hauptfenster angedockt werden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Main window not available for docking the user interface.</source>
        <translation type="unfinished">Das Hauptfenster ist zum Andocken nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>designer plugin widget (&apos;%1&apos;) could not be created</source>
        <translation type="unfinished">Das Designer-Plugin-Widget (&apos;%1&apos;) konnte nicht erstellt werden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>No designer plugin with className &apos;%s&apos; could be found. Please make sure that this plugin is compiled and the corresponding DLL and header files are in the designer folder</source>
        <translation type="unfinished">Es wurde kein Designer-Plugin mit dem &apos;className&apos; &apos;%s&apos; gefunden. Bitte stellen sie sicher, dass das Plugin kompiliert ist und die DLL mit der Header-Datei im Designer-Verzeichnis steht</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>dialog handle does not exist</source>
        <translation type="unfinished">Das Dialog-Handle existiert nicht</translation>
    </message>
    <message>
        <location line="+92"/>
        <location line="+25"/>
        <location line="+39"/>
        <location line="+47"/>
        <location line="+25"/>
        <location line="+25"/>
        <source>Dialog or plot handle does (not longer) exist. Maybe it has been closed before.</source>
        <translation type="unfinished">Das Dialog- oder Plot-Handle existiert nicht (mehr). Vielleicht wurde es zuvor geschlossen.</translation>
    </message>
    <message>
        <location line="-102"/>
        <source>dialog cannot be docked</source>
        <translation type="unfinished">Der Dialog kann nicht angedockt werden</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>dialog cannot be docked or undocked</source>
        <translation type="unfinished">Der Dialog kann nicht an- oder abgedockt werden</translation>
    </message>
    <message>
        <location line="+265"/>
        <source>defaultButton must be within enum QMessageBox::StandardButton</source>
        <translation type="unfinished">Der &apos;defaultButton&apos; muss innerhalb der Enumeration &apos;QMessageBox::StandardButton&apos; liegen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>buttons must be within enum QMessageBox::StandardButtons</source>
        <translation type="unfinished">&apos;buttons&apos; muss innerhalb der Enumeration &apos;QMessageBox::StandardButton&apos; liegen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>defaultButton must appear in buttons, too.</source>
        <translation type="unfinished">Der &apos;defaultButton&apos; muss ebenfalls in &apos;buttons&apos; erscheinen.</translation>
    </message>
    <message>
        <location line="+2466"/>
        <source>could not get figure with handle %i.</source>
        <translation type="unfinished">Es wurde kein Plot mit dem Handle %i gefunden.</translation>
    </message>
    <message>
        <location line="+67"/>
        <location line="+37"/>
        <location line="+31"/>
        <location line="+26"/>
        <source>the required widget does not exist (any more)</source>
        <translation type="unfinished">Das erforderliche Widget existiert nicht (mehr)</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>timer is invalid</source>
        <translation type="unfinished">Der Timer ist ungültig</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>could not find figure with given handle %1</source>
        <translation type="unfinished">Es wurde kein Plot mit dem Handle %1 gefunden</translation>
    </message>
    <message>
        <location line="-2507"/>
        <location line="+56"/>
        <source>property &apos;%1&apos; does not exist</source>
        <translation type="unfinished">Die Eigenschaft &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>property &apos;%1&apos; could not be read</source>
        <translation type="unfinished">Die Eigenschaft &apos;%1&apos; konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location line="+73"/>
        <location line="+29"/>
        <source>property &apos;%1&apos; could not be written</source>
        <translation type="unfinished">Die Eigenschaft &apos;%1&apos; konnte nicht beschrieben werden</translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+31"/>
        <source>The attribute number is out of range.</source>
        <translation type="unfinished">Die Anzahl der Attribute liegt außerhalb des Gültigkeitsbereichs.</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+31"/>
        <location line="+25"/>
        <location line="+24"/>
        <source>the objectID cannot be cast to a widget</source>
        <translation type="unfinished">Die Objekt-ID kann nicht auf ein Widget &apos;gecastet&apos; werden</translation>
    </message>
    <message>
        <location line="+181"/>
        <source>The parent widget is either unknown or does not exist any more.</source>
        <translation type="unfinished">Entweder existiert der &apos;Parent&apos; des Widgets nicht mehr oder ist unbekannt.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>The object ID is invalid.</source>
        <translation type="unfinished">Die ID des Datenobjekts ist ungültig.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The given object ID is unknown.</source>
        <translation type="unfinished">Die übergebene Objekt-ID ist unbekannt.</translation>
    </message>
    <message>
        <location line="+462"/>
        <source>object ID is not available</source>
        <translation type="unfinished">Die Objekt-ID ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-632"/>
        <source>could not get reference to main dialog or window</source>
        <translation type="unfinished">Es ist keine Referenz zum Hauptdialog oder -Fenster verfügbar</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>uiHandle is invalid</source>
        <translation type="unfinished">&quot;uiHandle&quot; ist ungültig</translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+51"/>
        <source>no object name given.</source>
        <translation type="unfinished">Es wurde kein Objektname angegeben.</translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+51"/>
        <source>The object ID of the parent widget is invalid.</source>
        <translation type="unfinished">Die Objekt-ID des übergeordneten Widget ist ungültig.</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>The object ID of the parent widget is unknown.</source>
        <translation type="unfinished">Die Objekt-ID der &apos;Parent&apos;-Widgets ist unbekannt.</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>parameter type %1 is unknown</source>
        <translation type="unfinished">Parametertyp &apos;%1&apos; ist unbekannt</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>signal does not exist</source>
        <translation type="unfinished">Das Signal existiert nicht</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>signal could not be connected to slot throwing a python keyboard interrupt.</source>
        <translation type="unfinished">Das Signal konnte nicht mit einem Slot verbunden werden.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>slot could not be found</source>
        <translation type="unfinished">Slot konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+1055"/>
        <source>unsupported data type</source>
        <translation type="unfinished">Nicht unterstützter Datentyp</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+63"/>
        <location line="+63"/>
        <source>figHandle %i is not handle for a figure window.</source>
        <translation type="unfinished">&quot;figHandle&quot; &apos;%i&apos; ist kein Handle eines Grafikfensters.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Figure %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="-2942"/>
        <location line="+2719"/>
        <location line="+63"/>
        <location line="+63"/>
        <location line="+185"/>
        <location line="+69"/>
        <source>figHandle %i not available.</source>
        <translation type="unfinished">&quot;figHandle&quot; &apos;%i&apos; ist nicht verfügbar.</translation>
    </message>
    <message>
        <location line="-2152"/>
        <location line="+93"/>
        <location line="+102"/>
        <location line="+147"/>
        <location line="+30"/>
        <location line="+51"/>
        <location line="+51"/>
        <location line="+145"/>
        <location line="+40"/>
        <location line="+66"/>
        <location line="+63"/>
        <source>The widget is not available (any more).</source>
        <translation type="unfinished">Das Widget ist (nicht länger) verfügbar.</translation>
    </message>
    <message>
        <location line="+606"/>
        <source>The requested widget does not exist (any more).</source>
        <translation type="unfinished">Das angeforderte Widget existiert nicht (mehr).</translation>
    </message>
    <message>
        <location line="+562"/>
        <source>figure window is not available any more</source>
        <translation type="unfinished">Grafikfenster ist nicht länger verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>handle &apos;%i&apos; is no figure.</source>
        <translation type="unfinished">Handle &apos;%i&apos; ist keine Grafik.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>handle &apos;0&apos; cannot be assigned.</source>
        <translation type="unfinished">Handle &apos;0&apos; ist ungültig.</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>subplot at indexed position %i is not available</source>
        <translation type="unfinished">Der Subplot an der indizierten Position %i ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+69"/>
        <source>figHandle %i is not a handle for a figure window.</source>
        <translation type="unfinished">&quot;figHandle&quot; &apos;%i&apos; ist kein Handle eines Grafikfensters.</translation>
    </message>
    <message>
        <location line="+123"/>
        <location line="+37"/>
        <location line="+37"/>
        <source>The desired widget has no signals/slots defined that enable the pick points interaction</source>
        <translation type="unfinished">Das gewünschte Widget hat keine gekoppelten Signale definiert, welche die Interaktion mit Pickern ermöglichen würden</translation>
    </message>
</context>
<context>
    <name>ito::UserInteractionWatcher</name>
    <message>
        <location filename="../organizer/userInteractionWatcher.cpp" line="+43"/>
        <source>The given shape storage is NULL.</source>
        <translation type="unfinished">Die übergebene geometrische Form hat den Wert NULL.</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+11"/>
        <source>The given widget does not have the necessary signals and slots for a user interaction.</source>
        <translation type="unfinished">Das übergebene Widget hat nicht die nötigen Signale und Slots für die Interaktion.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>User interaction terminated due to deletion of plot.</source>
        <translation type="unfinished">Die Benutzer-Interaktion wurde beendet da der Plot gelöscht wurde.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>User interaction aborted.</source>
        <translation type="unfinished">Die Benutzer-Interaktion wurde abgebrochen.</translation>
    </message>
</context>
<context>
    <name>ito::UserModel</name>
    <message>
        <location filename="../models/UserModel.cpp" line="+36"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>iniFile</source>
        <translation>INI-Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>features</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location line="+200"/>
        <location line="+14"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Administrator</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>File System</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User Management</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Addin Manager (Plugins)</source>
        <translation>Addin-Manager (Plugins)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Console</source>
        <translation>Konsole</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Console (Read Only)</source>
        <translation>Kondole (nur lesend)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Properties</source>
        <translation>Optionen</translation>
    </message>
</context>
<context>
    <name>ito::UserOrganizer</name>
    <message>
        <location filename="../organizer/userOrganizer.cpp" line="+57"/>
        <source>Standard User</source>
        <translation>Standardbenutzer</translation>
    </message>
    <message>
        <location line="+289"/>
        <source>file &apos;%s&apos; does not exist</source>
        <translation>Die Datei &apos;%s&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>itomSettings directory not found, aborting!</source>
        <translation>Das Verzeichnis &quot;itomSettings&quot; wurde nicht gefunden! Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Could not copy standard itom ini file!</source>
        <translation>Die Standard-Ini-Datei von itom konnte nicht kopiert werden!</translation>
    </message>
</context>
<context>
    <name>ito::UserUiDialog</name>
    <message>
        <location filename="../widgets/userUiDialog.cpp" line="+94"/>
        <source>filename &apos;%s&apos; does not exist</source>
        <translation>Dateiname &apos;%s&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>ui-file could not be correctly parsed.</source>
        <translation>UI-Datei konnte nicht korrekt geparst werden.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>itom</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>content-widget is empty.</source>
        <translation>Widget ist leer.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>dialog button role is unknown or not supported</source>
        <translation>Die Rolle des Dialog-Buttons ist unbekannt oder wird nicht unterstützt</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropEditorAPI</name>
    <message>
        <source>Base path for relative pathes: </source>
        <translation type="vanished">Basispfad der relativen Pfade: </translation>
    </message>
    <message>
        <source>[does not exist]</source>
        <translation type="vanished">[existiert nicht]</translation>
    </message>
    <message>
        <source>Load python api file</source>
        <translation type="vanished">Pyhton API-Dateien laden</translation>
    </message>
    <message>
        <source>Python api file (*.api)</source>
        <translation type="vanished">Pyhton API-Dateien (*.api)</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropEditorScripts</name>
    <message>
        <location filename="../ui/widgetPropEditorScripts.cpp" line="+130"/>
        <source>Line color:</source>
        <translation>Linienfarbe:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Background color:</source>
        <translation>Hintergrundfarbe:</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropEditorStyles</name>
    <message>
        <location filename="../ui/widgetPropEditorStyles.cpp" line="+90"/>
        <source>Paper color</source>
        <translation>Papierfarbe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caret color (Foreground: cursor color, Background: color of current line)</source>
        <translation>Farbe des Text-Cursors (Fordergrund: Cursor-Farbe, Hintergrund: Farbe der aktuellen Zeile)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fold margin color</source>
        <translation>Farbe der Gliederungsspalte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Margin color</source>
        <translation>Randfarbe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Whitespace color (if whitespace characters are visible)</source>
        <translation>Farbe der Leerräume (wenn Leerräume sichtbar sind)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unmatched brace color</source>
        <translation>Farbe der Klammer (bei fehlender Gegenklammer)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Matched brace color</source>
        <translation>Farbe der Klammern (Gegenklammer vorhanden)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command line: Background for error messages</source>
        <translation>Kommandozeile: Hintergrundfarbe von Fehlermeldungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command line: Background for currently executed line</source>
        <translation>Kommandozeile: Hintergrundfarbe der aktuell ausgeführten Zeile</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command line: Background for python input</source>
        <translation>Kommandozeile: Hintergrundfarbe einer Eingabezeile (Python-Befehl: input)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Script: Background for erroneous line</source>
        <translation>Skript: Hintergrund für fehlerhafte Zeilen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Background color and text color of current selection</source>
        <translation>Hintergrundfarbe und Textfarbe für aktuelle Selektion</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Background color of words equal to the currently selected string</source>
        <translation>Hintergrundfarbe aller Wörter, die identisch zu dem aktuell ausgewählten Wort sind</translation>
    </message>
    <message>
        <location line="+180"/>
        <location line="+88"/>
        <source>Sample Text</source>
        <translation>Beispieltext</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+5"/>
        <source>color: %1; background-color: %2;</source>
        <translation>Farbe: %1, Hintergrundfarbe: %2;</translation>
    </message>
    <message>
        <location line="+240"/>
        <source>Import style file</source>
        <translation>Style importieren</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>File does not exist.</source>
        <translation>Datei existiert nicht.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file &apos;%1&apos; does not exist.</source>
        <translation>Die Datei &apos;%1&apos; existiert nicht.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>File does readable.</source>
        <translation>Die Datei ist schreibgeschützt.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file &apos;%1&apos; cannot be opened.</source>
        <translation>Die Datei &apos;%1&apos; kann nicht geöffnet werden.</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+90"/>
        <location line="+5"/>
        <source>Error reading xml file.</source>
        <translation>Fehler beim Lesen der XML-Datei.</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>The content of the file &apos;%1&apos; seems to be corrupt.</source>
        <translation>Der Inhalt der Datei &apos;%1&apos; scheint beschädigt zu sein.</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Missing node &apos;LexerStyles&apos; as child of &apos;NotepadPlus&apos; in xml file.</source>
        <translation>Der Knoten &apos;LexerStyles&apos; als Kind von &apos;NotepadPlus&apos; wurde in der XML-Datei nicht gefunden.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Missing node &apos;LexerType&apos; with name &apos;python&apos; as child of &apos;LexerStyles&apos; in xml file.</source>
        <translation>Der Knoten &apos;LexerStyles&apos; mit dem Namen &apos;python&apos; als Kind von &apos;NotepadPlus&apos; wurde in der XML-Datei nicht gefunden.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Missing node &apos;GlobalStyles&apos; as child of &apos;NotepadPlus&apos; in xml file.</source>
        <translation>Der Knoten &apos;GlobalStyles&apos; als Kind von &apos;NotepadPlus&apos; wurde in der XML-Datei nicht gefunden.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The file is not a Notepad++ style file.</source>
        <translation>Die Datei ist keine Notepad++-Style-Datei.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The content of the file &apos;%1&apos; could not be properly analyzed (%2): %3</source>
        <translation>Der Inhalt der Datei &apos;%1&apos; kann nicht vollständig analysiert werden (%2): %3</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The content of the file &apos;%1&apos; could not be properly analyzed (%2): %3 in line %4, column %5</source>
        <translation>Der Inhalt der Datei &apos;%1&apos; kann nicht vollständig analysiert werden (%2): %3 in Zeile %4, Spalte %5</translation>
    </message>
    <message>
        <location line="+234"/>
        <source>Export style data</source>
        <translation>Style exportieren</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropFigurePlugins</name>
    <message>
        <location filename="../ui/widgetPropFigurePlugins.cpp" line="+79"/>
        <source>class name</source>
        <translation>Klassenname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>data types</source>
        <translation>Datentyp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>data formats</source>
        <translation>Datenformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>features</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>plot type</source>
        <translation>Plot-Typ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>plugin file</source>
        <translation>Plugin-Datei</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>default figure plot</source>
        <translation>Standardgrafik-Plot</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropGeneralApplication</name>
    <message>
        <location filename="../ui/widgetPropGeneralApplication.cpp" line="+155"/>
        <source>load directory</source>
        <translation>Verzeichnis hinzufügen</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropGeneralLanguage</name>
    <message>
        <location filename="../ui/widgetPropGeneralLanguage.cpp" line="+115"/>
        <location line="+20"/>
        <source>Current Language: </source>
        <translation>Aktuelle Sprache: </translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropHelpDock</name>
    <message>
        <location filename="../ui/widgetPropHelpDock.cpp" line="+62"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Updates and Downloads</source>
        <translation>Updates und Downloads</translation>
    </message>
    <message>
        <location line="+258"/>
        <location line="+467"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-413"/>
        <source>Download website seems to be in an temporary offline mode.</source>
        <translation type="unfinished">Die Download-Seite scheint momentan offline zu sein.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Invalid type attribute of xml file</source>
        <translation>Ungültiges Typen-Attribut der XML-Datei</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Type attribute node &apos;database&apos; of xml file is missing.</source>
        <translation>Typen-Attribut des Knotens &apos;database&apos; der XML-Dateien vermisst.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>xml parsing error: %1</source>
        <translation>XML-Parserfehler: %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>xml error: node &apos;database&apos; is missing.</source>
        <translation>XML-Fehler: Knoten &apos;database&apos; nicht gefunden.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Up to date</source>
        <translation>Aktuell</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Update to version: %1 (%2)</source>
        <translation>Update auf Version: %1 (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Download version: %1 (%2)</source>
        <translation>Download-Version: %1 (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>wrong Scheme: %1 (your scheme %2)</source>
        <translation>Falsches Schema: %1 (aktuelles Schema %2)</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Remote</source>
        <translation></translation>
    </message>
    <message>
        <location line="+141"/>
        <source>download error</source>
        <translation>Download-Fehler</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Download update</source>
        <translation>Update Download</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Locate on disk</source>
        <translation type="unfinished">Lokal auf die Festplatte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove from disk</source>
        <translation type="unfinished">Von der Festplatte löschen</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Remove script reference</source>
        <translation type="unfinished">Skriptreferenz löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you really want to delete the file &apos;%s&apos;?</source>
        <translation type="unfinished">Soll die Datei &apos;%s&apos; wirklich gelöscht werden?</translation>
    </message>
    <message>
        <source>&amp;update</source>
        <translation type="vanished">&amp;Update</translation>
    </message>
    <message>
        <source>locate on disk</source>
        <translation type="vanished">Verzeichnis anzeigen</translation>
    </message>
    <message>
        <source>remove from disk</source>
        <translation type="vanished">Auf der Festplatte löschen</translation>
    </message>
    <message>
        <location line="-103"/>
        <source>Remote database update...</source>
        <translation>Remote Datenbank-Update...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Timeout: Server is not responding in time</source>
        <translation>Zeitüberschreitung: Server antwortet nicht</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Could not delete old local version of Database</source>
        <translation>Die alte Datenbankversion konnte nicht gelöscht werden</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropPalettes</name>
    <message>
        <location filename="../ui/widgetPropPalettes.cpp" line="+399"/>
        <location line="+81"/>
        <location line="+50"/>
        <location line="+150"/>
        <source>Color palette altered</source>
        <translation>Farbpalette geändert</translation>
    </message>
    <message>
        <location line="-280"/>
        <location line="+81"/>
        <location line="+50"/>
        <source>The current color palette was altered and is currently unsaved. Save changes or discard?</source>
        <translation>Die aktuelle Farbpalette wurde geändert und derzeit nicht gespeichert. Änderungen speichern oder verwerfen?</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>User Palette</source>
        <translation>Benutzerdefinierte Farbpalette</translation>
    </message>
    <message>
        <location line="+52"/>
        <source> - Copy</source>
        <translation> - Kopie</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Palette is read only</source>
        <translation type="unfinished">Die Farbpalette ist schreibgeschützt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Palette is read only and cannot be removed!</source>
        <translation>Die Farbpalette ist schreibgeschützt und kann nicht entfernt werden!</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>name conflict</source>
        <translation>Namenskonflikt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Another color palette with the same name already exists.</source>
        <translation>Eine andere Farbpalette mit dem gleichen Namen existiert bereits.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>invalid name</source>
        <translation>Ungültiger Name</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>An empty palette name is not valid.</source>
        <translation>Eine leere Farbpalette ist ungültig.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>The current color palette was altered and is currently unsaved. Save changes or ignore?</source>
        <translation>Die aktuelle Farbpalette wurde geändert und derzeit nicht gespeichert. Änderungen speichern oder ignorieren?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error saving current color palette.</source>
        <translation>Fehler beim Speichern der aktuellen Farbpalette.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The current color palette could not be saved.</source>
        <translation>Die aktuelle Farbpalette konnte nicht gespeichert werden.</translation>
    </message>
    <message>
        <location line="+106"/>
        <location line="+5"/>
        <source>Remove color stop</source>
        <translation type="unfinished">Knoten entfernen</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>A color palette must have at least two color stops.</source>
        <translation type="unfinished">Eine Farbpalette muss mindestens zwei Knoten haben.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+33"/>
        <source>No color stop has been selected.</source>
        <translation type="unfinished">Es wurde kein Knoten ausgewählt.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Add color stop</source>
        <translation type="unfinished">Knoten hinzufügen</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Color palette import</source>
        <translation>Farbpalette importieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+66"/>
        <source>Itom color palette (*.icp)</source>
        <translation type="unfinished">itom Farbpalette (*.icp)</translation>
    </message>
    <message>
        <location line="-53"/>
        <location line="+8"/>
        <source>Wrong file format</source>
        <translation>Falsches Dateiformat</translation>
    </message>
    <message>
        <location line="-8"/>
        <location line="+8"/>
        <source>The color palette &apos;%1&apos; in the itom color palette file is no valid color palette</source>
        <translation>Die Farbpalette &apos;%1&apos; in der Datei ist keine gültiges Format für Paletten</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Name already exists</source>
        <translation type="unfinished">Dieser Name existiert bereits</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The name &apos;%1&apos; of the color palette already exists. Please indicate a new name to load the color palette:</source>
        <translation>Der Name &apos;%1&apos; der Farbpalette existiert bereits. Bitte einen neuen Namen angeben um die Farbpalette zu laden:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color palette export</source>
        <translation>Farbpaletten Export</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropPythonStartup</name>
    <message>
        <source>Base path for relative pathes: </source>
        <translation type="vanished">Basispfad der relativen Pfade: </translation>
    </message>
    <message>
        <location filename="../ui/widgetPropPythonStartup.cpp" line="+99"/>
        <source>Load python script</source>
        <translation>Pyhton-Skript laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python script (*.py)</source>
        <translation>Python-Skript (*.py)</translation>
    </message>
</context>
<context>
    <name>ito::WorkspaceDockWidget</name>
    <message>
        <location filename="../widgets/workspaceDockWidget.cpp" line="+151"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Delete Selected Item(s)</source>
        <translation>Selektierte(s) Objekt(e) löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Export Selected Item(s)</source>
        <translation>Selektierte(s) Objekt(e) exportieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import Item(s)</source>
        <translation>Objekt(e) importieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename Selected Item</source>
        <translation>Selektiertes Objekt umbenennen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>1D Line Plot</source>
        <translation>1D Linienplot</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2D Image Plot</source>
        <translation>2D Bildplot</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2.5D Isometric Plot</source>
        <translation>2,5D isometrischer Plot</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3D Cloud Or Mesh Visualization</source>
        <translation>3D Wolken- oder Netzvisualisierung</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unpack Loaded Dictionary</source>
        <translation>In  &apos;Dictionary&apos; entpacken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unpack loaded dictionary from idc or mat files to workspace</source>
        <translation>Entpacke importierte idc- oder mat-Dateien in ein neues &apos;Dictionary&apos; des Workspaces</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear All Variables</source>
        <translation type="unfinished">Alle Variablen bereinigen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Workspace</source>
        <translation></translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Do you really want to delete the selected variables?</source>
        <translation>Sollen die markierten Variablen wirklich gelöscht werden?</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Export data</source>
        <translation>Datenexport</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error while exporting variables:
%1</source>
        <translation>Fehler beim exportieren der Variablen:
%1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Import data</source>
        <translation>Datenimport</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error while importing variables:
%1</source>
        <translation>Fehler beim importieren der Variablen:
%1</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+150"/>
        <source>Python engine not available</source>
        <translation>Die Python-Engine ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-146"/>
        <location line="+150"/>
        <source>Variables cannot be plot since python is busy right now</source>
        <translation>Die Variable kann nicht angezeigt werden während Python beschäftigt ist</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Workspace not available</source>
        <translation>Workspace ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Nothing selected</source>
        <translation>Es wurde nichts ausgewählt</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Timeout while getting value from workspace</source>
        <translation>Zeitüberschreitung beim Lesen der Werte aus dem Workspace</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The number of values returned from workspace does not correspond to requested number</source>
        <translation>Die Anzahl der zurückgegebenen Werte aus dem Workspace entspricht nicht der angeforderten Anzahl</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Timeout while renaming variables</source>
        <translation>Zeitüberschreitung beim umbenennen der Variablen</translation>
    </message>
    <message>
        <location line="-269"/>
        <source>At least one variable cannot be plotted since it is no dataObject or numpy.array. These values are ignored.</source>
        <translation>Mindestens eine Variable kann nicht angezeigt werden, da diese kein DataObject oder &apos;Numpy.Array&apos; ist. Dieser Wert wird ignoriert.</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Invalid or unsupported data type for plotting.</source>
        <translation>Ungültiger oder nicht unterstützter Datentyp zum Anzeigen.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Timeout while plotting dataObject or numpy.array</source>
        <translation>Zeitüberschreitung beim Anzeigen eines  DataObjects oder &apos;Numpy.Array&apos;s</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+7"/>
        <source>Plot data</source>
        <translation>Daten anzeigen</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Error while plotting value(s):
%1</source>
        <translation>Fehler beim plotten von Daten:
%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning while plotting value(s):
%1</source>
        <translation>Warnung beim plotten von Daten:
%1</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>renaming variable</source>
        <translation>Variable umbenennen</translation>
    </message>
</context>
<context>
    <name>ito::WorkspaceWidget</name>
    <message>
        <location filename="../widgets/workspaceWidget.cpp" line="+65"/>
        <source>Globals</source>
        <translation>Global</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+5"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location line="-5"/>
        <location line="+5"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Locals</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location line="+403"/>
        <source>timeout while asking python for detailed information</source>
        <translation>Zeitüberschreitung bei der Anfrage an Python für detailierte Informationen</translation>
    </message>
</context>
<context>
    <name>paramInputDialog</name>
    <message>
        <location filename="../ui/paramInputDialog.ui" line="+14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Items List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>New Item</source>
        <translation>Neuer Eintrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete Item</source>
        <translation>Eintrag löschen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Move Item Up</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>U</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Move Item Down</source>
        <translation>Abwärts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>D</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>uiDebugViewer</name>
    <message>
        <location filename="../widgets/uiDebugViewer.ui" line="+17"/>
        <source>Debug-Viewer</source>
        <translation>Debug-Übersicht</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>global workspace variables</source>
        <translation>Variablen im globalen Workspace</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>local</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>PushButton</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>uiMainWindow</name>
    <message>
        <location filename="../widgets/uiMainWindow.ui" line="+14"/>
        <source>itom</source>
        <translation>itom</translation>
    </message>
</context>
<context>
    <name>userManagement</name>
    <message>
        <location filename="../widgets/userManagement.ui" line="+67"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Ini File</source>
        <translation>Ini-Datei</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>New User</source>
        <translation>Neuer Benutzer erstellen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Delete User</source>
        <translation>Benutzer löschen</translation>
    </message>
    <message>
        <location line="-126"/>
        <source>User Management</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Permission</source>
        <translation>Berechtigungen</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Edit User</source>
        <translation>Benutzer bearbeiten</translation>
    </message>
</context>
<context>
    <name>userManagementEdit</name>
    <message>
        <location filename="../widgets/userManagementEdit.ui" line="+20"/>
        <source>User Management Edit / New</source>
        <translation></translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+194"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="-188"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Uses the current Windows / Linux user as name.</source>
        <translation type="unfinished">Übernimmt den aktuell angemeldeten Windows/Linux-Benutzer als Name.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set Current OS User</source>
        <translation type="unfinished">Aktueller System-User setzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>If checked, the ID (filename suffix of the settings file) is generated from the name</source>
        <translation type="unfinished">Wenn aktiviert, wird die ID (Dateiendung der Einstellungsdatei) aus dem Namen generiert</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This password provides a basic protection from loading itom with this user profile.</source>
        <translation type="unfinished">Diese Passwort stellt einen rudimentären Schutz zum Benutzen dieses Profils dar.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Main Application Permission</source>
        <translation>Berechtigungen in der Hauptanwendung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Developer Tools (Workspace, Breakpoints, Ui-Designer, Call Stack)</source>
        <translation>Entwickler-Tools (Workspace, Haltepunkte,  Ui-Designer, Call Stack)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit Properties</source>
        <translation>Optionen ändern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Addin Manager Widget (Plugins)</source>
        <translation>Addin-Manager (Plugins)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>File System Widget</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User Management</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Console</source>
        <translation>Konsole</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>read only</source>
        <translation>nur lesen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>off</source>
        <translation>aus</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Developer</source>
        <translation></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Startup Scripts</source>
        <translation type="unfinished">Skripts im Autostart</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Add File</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Move Up</source>
        <translation>Nach Oben</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Move Down</source>
        <translation>Nach Unten</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Add new files relative to itom base path: %1</source>
        <translation type="unfinished">Neue Dateien relativ zum itom Hauptverzeichnis laden: %1</translation>
    </message>
    <message>
        <location line="-161"/>
        <source>Administrator</source>
        <translation></translation>
    </message>
</context>
</TS>
